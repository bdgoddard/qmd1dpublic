function [V1,D1V1,D2V1] = V1Default2D(Parameters,varargin)
% One of the two diabatic potential surfaces.

    if(isfield(Parameters,'alpha1'))
        alpha = Parameters.alpha1;
    else
        alpha = 0.5;
    end

    x1 = varargin{1};
    
    V1 = alpha * tanh(x1);

    D1V1 = alpha * sech(x1).^2;
    D2V1 = zeros(size(x1));


end