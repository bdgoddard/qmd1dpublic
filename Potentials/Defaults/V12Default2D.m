function [V12,D1V12,D2V12] = V12Default2D(Parameters,varargin)

% The diabatic coupling element
    
    if(isfield(Parameters,'alpha12'))
        alpha = Parameters.alpha12;
    else
        alpha = 0.5;
    end

    if(isfield(Parameters,'beta12'))
        beta = Parameters.beta12;
    else
        beta = 0.5;
    end
    
    if isfield(Parameters,'gamma12')
        gamma=Parameters.gamma12;
    else
        gamma=0;
    end
    

    x1 = varargin{1};
    x2 = varargin{2};

    expPart = exp(-(x1.^2 + alpha*(x2-gamma).^2)/2);

    V12 = 1 - beta * expPart;

    D1V12 = beta * x1 .* expPart ;
    D2V12 = beta * alpha *  x2 .* expPart ;
    
end