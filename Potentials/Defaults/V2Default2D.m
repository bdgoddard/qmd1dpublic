function [V2,D1V2,D2V2] = V2Default2D(Parameters,varargin)

    [V1,D1V1,D2V1] = V1Default2D(Parameters,varargin{:});
    
    V2 = -V1;
    D1V2 = -D1V1;
    D2V2 = -D2V1;
    
end