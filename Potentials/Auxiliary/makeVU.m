function VU=makeVU(Potentials,varargin)

[Rho,~,~,Trace]=makeRhoXZTrace(Potentials,varargin{:});

VU = Rho + Trace;

end