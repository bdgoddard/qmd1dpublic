function [V1,DV1,DDV1] = V1LZSimple(Parameters,varargin)

alpha = Parameters.alpha;

x = varargin{1};

V1 = alpha * tanh(x);

DV1 = alpha * sech(x).^2;
DDV1 = - 2 * alpha * tanh(x) .* sech(x).^2;
end
