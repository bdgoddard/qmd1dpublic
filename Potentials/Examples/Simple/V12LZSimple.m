function [V12,DV12,DDV12] = V12LZSimple(Parameters,varargin)


delta = Parameters.delta;

x = varargin{1};

V12 = delta * ones(size(x));

DV12 = zeros(size(x));
DDV12 = zeros(size(x));
end