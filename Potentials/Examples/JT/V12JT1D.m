function [V12,DV12,DDV12] = V12JT1D(Parameters,varargin)

if(isfield(Parameters,'delta'))
    delta = Parameters.delta;
else
    delta = 0.5;
end

x = varargin{1};

V12 = delta*ones(size(x));
DV12 = zeros(size(x));
DDV12 = zeros(size(x));

end