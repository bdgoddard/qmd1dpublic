function [V1,DV1,DDV1] = V1JT1D(Parameters,varargin)

x = varargin{1};

V1 = x;
DV1 = ones(size(x));
DDV1 = zeros(size(x));

end