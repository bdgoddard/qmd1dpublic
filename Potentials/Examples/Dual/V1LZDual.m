function [V1,DV1,DDV1] = V1LZDual(Parameters,varargin)

alpha = Parameters.alpha;
x0 = Parameters.x0;

x = varargin{1};

V1 = alpha*( tanh(x-x0) + tanh(-(x+x0)) + 1 );

DV1 = alpha * ( sech(x-x0).^2 - sech(x+x0).^2 );
DDV1 = 2*alpha* (tanh(x+x0).*sech(x+x0).^2 - tanh(x-x0).*sech(x-x0).^2);

end
