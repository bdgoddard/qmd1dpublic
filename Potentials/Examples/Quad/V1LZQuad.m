function [V1,DV1,DDV1] = V1LZQuad(Parameters,varargin)

alpha = Parameters.alpha;
beta = Parameters.beta;

x = varargin{1};

V1 = alpha * tanh(x) + beta* x.^2;

% Note that derivative shouldn't contain the V0 part 

DV1 = alpha * sech(x).^2;
DDV1 = - 2 * alpha * tanh(x) .* sech(x).^2;

%DV1 = alpha * sech(x).^2 + 2* beta * x;
%DDV1 = - 2 * alpha * tanh(x) .* sech(x).^2 + 2*beta;
end
