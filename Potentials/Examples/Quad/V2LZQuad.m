function V2=V2LZQuad(Parameters,varargin)

alpha = Parameters.alpha;
beta = Parameters.beta;

x = varargin{1};

V2 = -alpha * tanh(x) + beta* x.^2;

DV2 = [];
DDV2 = [];
end