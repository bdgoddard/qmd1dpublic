clear all

expList = {'ExpFormula1D','ExpLZD1D'};
nExp = length(expList);

for iExp = 1:nExp

    OutputOptions.PlotAnimation = 0;
    OutputOptions.Waitbar = 1;

    OutputOptions.DataSubDir = ['LZPaper' filesep 'Quad'];

    Numerics.epsilon = 1/50;
    Numerics.NumPoints = 2^14;

    Numerics.xStart = -40;
    Numerics.xEnd = 40;

    Numerics.d = 1;

    Potentials.V1 = 'V1LZQuad';
    Potentials.V2 = 'V2LZQuad';
    Potentials.V12 = 'V12LZQuad';

    Potentials.Parameters.alpha = 0.5;
    Potentials.Parameters.beta  = 0.05;
    %Potentials.Parameters.beta  = 0.0;
    Potentials.Parameters.delta = 0.5;

    p0 = 5;
    Numerics.timestep = 1/100/p0;

    Numerics.formulaOptions.direction = 'up';
    %Numerics.formulaOptions.direction = 'down';

    Numerics.formulaOptions.ExpFunction = expList{iExp};

    if(strcmp(Numerics.formulaOptions.direction,'down'))
        Wavepackets.PsiHatUpCrossing = 'GaussianHat1D';
    else
        Wavepackets.PsiHatDownCrossing = 'GaussianHat1D';
    end    

    Wavepackets.Parameters.q0 = 0;
    Wavepackets.Parameters.p0 = p0;
    Numerics.tBackward = -25/p0;
    %Numerics.tForward = 25/p0;
    Numerics.tForward = 125/p0;

    Numerics.formulaOptions.relativeCutoff = 10^(-6);

    WPNIn = packWPN(Wavepackets,Potentials,Numerics);

    [WPN(iExp),OutputOptions,RelErr(iExp),RelAbsErr(iExp)] = ExactVsFormulaMultiple(WPNIn,OutputOptions);

end
    
[Wavepackets,Potentials,Numerics] = unpackWPN(WPN(1));

mF = Wavepackets.MassFormula;
mE = Wavepackets.MassUp;
times = (0: (length(mE)-1))*Numerics.timestep;

[Wavepackets,Potentials,Numerics] = unpackWPN(WPN(2));
mLZ = Wavepackets.MassFormula;

figure('Position',[200,200,1200,800])

plot(times, mF ,'r')
hold on
plot(times, mLZ ,'--b')
plot(times, mE, ':k')
ylim([0,2*max(mF)])
xlabel('Time')
ylabel('Transmitted Mass')
pdfFile = [OutputOptions.DataDirO filesep 'LZQuadMass.pdf'];
save2pdf(pdfFile,gcf);

