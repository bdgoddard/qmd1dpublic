clear all

epsilonList = [1/50,1/40,1/30,1/20,1/10];
nEpsilon    = length(epsilonList);

deltaList = 0.05:0.05:1;
nDelta = length(deltaList);

p0List = 3:0.5:10;
np0 = length(p0List);

%expList = {'ExpFormula1D','ExpLZD1D','ExpLZA1D'};
expList = {'ExpFormula1D','ExpLZA1D'};
nExp = length(expList);

dirList = {'down'};
nDir = length(dirList);

RelativeErrors = zeros(nEpsilon,nDelta,np0,nDir,nExp);
MassErrors = zeros(nEpsilon,nDelta,np0,nDir,nExp);

for iEpsilon = 1:nEpsilon
    for iDelta = 1:nDelta
        for ip0 = 1:np0
            for iDir = 1:nDir
                for iExp = 1:nExp

                    tic
                    
                    disp([  num2str(epsilonList(iEpsilon)) ' ' ...
                            num2str(deltaList(iDelta)) ' ' ...
                            num2str(p0List(ip0)) ' ' ...
                            dirList{iDir} ' ' ...
                            expList{iExp}]);
                    
                    OutputOptions.PlotAnimation = 0;
                    OutputOptions.Waitbar = 1;
                    OutputOptions.DataSubDir = ['LZPaper' filesep 'Simple'];

                    Numerics.epsilon = epsilonList(iEpsilon);
                    Numerics.NumPoints = 2^14;

                    Numerics.xStart = -40;
                    Numerics.xEnd = 40;

                    Numerics.d = 1;

                    Potentials.V1 = 'V1LZSimple';
                    Potentials.V2 = 'V2LZSimple';
                    Potentials.V12 = 'V12LZSimple';

                    Potentials.Parameters.alpha = 0.5;

                    Potentials.Parameters.delta = deltaList(iDelta);

                    p0 = p0List(ip0);
                    Numerics.timestep = 1/100/p0;
                    %Numerics.timestep = 1/10/p0;

                    Numerics.formulaOptions.direction = dirList{iDir};

                    Numerics.formulaOptions.ExpFunction = expList{iExp};

                    if(strcmp(Numerics.formulaOptions.direction,'down'))
                        Wavepackets.PsiHatUpCrossing = 'GaussianHat1D';
                    else
                        Wavepackets.PsiHatDownCrossing = 'GaussianHat1D';
                    end    

                    Wavepackets.Parameters.q0 = 0;
                    Wavepackets.Parameters.p0 = p0;
                    Numerics.tBackward = -20/p0;
                    Numerics.tForward = 20/p0;

                    Numerics.formulaOptions.relativeCutoff = 10^(-6);

                    
                    WPN = packWPN(Wavepackets,Potentials,Numerics);
                                        

                    [WPN,OutputOptions,RelError,MassError] = ExactVsFormula(WPN,OutputOptions);

                    RelativeErrors(iEpsilon,iDelta,ip0,iDir,iExp) = RelError;
                    MassErrors(iEpsilon,iDelta,ip0,iDir,iExp) = MassError;
                    
                    toc
                    
                end
            end
        end
    end
end

