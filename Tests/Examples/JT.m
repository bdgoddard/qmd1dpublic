% Comparison between exact and formula for different momenta
clear all

deltaList = 0.05:0.05:1;
%deltaList = 1;

nDelta = length(deltaList);

%expList = {'ExpFormula1D','ExpLZD1D','ExpLZA1D'};
expList = {'ExpFormula1D','ExpLZD1D'};
nExp = length(expList);

RelativeErrors = zeros(nDelta,nExp);
MassErrors = zeros(nDelta,nExp);
MassDown   = zeros(nDelta,1);

for iDelta = 1:nDelta

    for iExp = 1:nExp

        OutputOptions.PlotAnimation = 0;
        OutputOptions.Waitbar = 1;
        OutputOptions.DataSubDir = ['LZPaper' filesep 'JT'];

        Wavepackets = struct;
        Potentials = struct;
        Numerics = struct;

        Numerics.epsilon = 1/50;
        Numerics.NumPoints = 2^15;

%         Numerics.xStart = -50;
%         Numerics.xEnd = 50;

        Numerics.xStart = -60;
        Numerics.xEnd = 60;


        Numerics.d = 1;

        Potentials.V1 = 'V1JT1D';
        Potentials.V2 = 'V2JT1D';
        Potentials.V12 = 'V12JT1D';

        Potentials.Parameters.delta = deltaList(iDelta);

        %working example for down
        p0 = 8;
        Numerics.timestep = 1/1000/p0;
        
        %d0 = 35;
        d0 = 40;
        
        Numerics.tBackward = -d0/p0;
        Numerics.tForward = d0/p0;



        Numerics.formulaOptions.direction = 'down';
        Numerics.formulaOptions.ExpFunction = expList{iExp};
        
        fromInitial = false;

        if(strcmp(Numerics.formulaOptions.direction,'down'))
            if(fromInitial)
                Wavepackets.PsiUpInitial = 'Gaussian1D';
            else
                Wavepackets.PsiHatUpCrossing = 'GaussianHat1D';
            end
        else
            if(fromInitial)
                Wavepackets.PsiDownInitial = 'Gaussian1D';
            else
                Wavepackets.PsiHatDownCrossing = 'GaussianHat1D';
            end
        end    

        Wavepackets.Parameters.q0 = 0;
        Wavepackets.Parameters.p0 = p0;

        Numerics.formulaOptions.relativeCutoff = 10^(-6);
    
        WPN = packWPN(Wavepackets,Potentials,Numerics);

        [WPN,OutputOptions,Error,MassError] = ExactVsFormula(WPN,OutputOptions);
        
        RelativeErrors(iDelta,iExp) = Error;
        MassErrors(iDelta,iExp) = MassError;
        MassDown(iDelta) = WPN.Wavepackets.MassDown(end);
        
    end
        
end

figure
plot(deltaList,RelativeErrors(:,1),'r')
hold on
plot(deltaList,RelativeErrors(:,2),'b--')
xlabel('$\delta$','Interpreter','latex');
ylabel('Relative Error','Interpreter','latex');
pdfFile = [OutputOptions.DataDirO filesep 'LZJTError.pdf'];
save2pdf(pdfFile,gcf);

figure
plot(deltaList,MassDown,'k');
set(gca,'YScale','log');
xlabel('$\delta$','Interpreter','latex');
ylabel('Transmitted Mass','Interpreter','latex');
pdfFile = [OutputOptions.DataDirO filesep 'LZJTMass.pdf'];
save2pdf(pdfFile,gcf);
