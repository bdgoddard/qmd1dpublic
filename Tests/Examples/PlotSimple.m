function PlotLZSimple(RelativeErrors,MassErrors,epsilonList, deltaList, p0List, expList, dirList,OutputOptions)

% RelativeErrors = zeros(nEpsilon,nDelta,np0,nDir,nExp);
% MassErrors = zeros(nEpsilon,nDelta,np0,nDir,nExp);

    close all

    nEpsilon = length(epsilonList);
    nDelta = length(deltaList);
    np0 = length(p0List);
    nExp = length(expList);
    nDir = length(dirList);
    
    pdfDir = OutputOptions(1).DataDirO;
    
    hf = figure('Position',[200,200,1200,800]);
    hLZ = figure('Position',[200,200,1200,800]);
    
    p0Plot = 1:2:np0;
    np0Plot = length(p0Plot);
    
    for ip0 = 1:np0Plot
    
        figure(hf)
        subplot(2,ceil(np0Plot/2),ip0)

        for iEpsilon = 1:nEpsilon
            plot(deltaList,RelativeErrors(iEpsilon,:,p0Plot(ip0),1,1));
            hold on
        end

        ylim([0,0.1]);
        xlim([0,1]);
        if(ip0 == 1 || ip0 == np0Plot/2 + 1)
            ylabel('Relative Error');
        end
        if(ip0 > np0Plot/2)
            xlabel('$\delta$','Interpreter','latex');
        end
        title(['$ p_0 = ' num2str(p0List(p0Plot(ip0))) '$'],'Interpreter','latex');
                      
        %legend(num2str(epsilonList'));
        
        
        figure(hLZ)
        subplot(2,ceil(np0Plot/2),ip0)

        for iEpsilon = 1:nEpsilon
            plot(deltaList,RelativeErrors(iEpsilon,:,p0Plot(ip0),1,2));
            hold on
        end

        %ylim([0,2]);
        ylim([0,0.1]);
        xlim([0,1]);
        if(ip0 == 1 || ip0 == np0Plot/2 + 1)
            ylabel('Relative Error');
        end
        if(ip0 > np0Plot/2)
            xlabel('$\delta$','Interpreter','latex');
        end
        title(['$ p_0 = ' num2str(p0List(p0Plot(ip0))) '$'],'Interpreter','latex');
       
    end

%     pdfFile = [pdfDir filesep 'FormulaDelta.pdf'];
%     save2pdf(pdfFile,hf);
% 
    pdfFile = [pdfDir filesep 'LZDeltaZoom.pdf'];
    save2pdf(pdfFile,hLZ);
    
end

