clear all

OutputOptions.PlotAnimation = 1;
OutputOptions.Waitbar = 1;
OutputOptions.DataSubDir = ['LZPaper' filesep 'SimpleNG'];

Numerics.epsilon = 1/50;
Numerics.NumPoints = 2^14;

Numerics.xStart = -40;
Numerics.xEnd = 40;

Numerics.d = 1;

Potentials.V1 = 'V1LZSimple';
Potentials.V2 = 'V2LZSimple';
Potentials.V12 = 'V12LZSimple';

Potentials.Parameters.alpha = 0.5;

Potentials.Parameters.delta = 0.5;

p0Temp = 5;
Numerics.timestep = 1/100/p0Temp;

Numerics.formulaOptions.direction = 'down';

Numerics.formulaOptions.ExpFunction = 'ExpFormula1D';

if(strcmp(Numerics.formulaOptions.direction,'down'))
    Wavepackets.PsiHatUpCrossing = 'MultipleGaussianHat1D';
else
    Wavepackets.PsiHatDownCrossing = 'MultipleGaussianHat1D';
end    

Wavepackets.Parameters.q0 = [0.1, 0, -0.05];
Wavepackets.Parameters.p0 = [4.6, 5, 5.3];
Wavepackets.Parameters.w = [0.7, 1, 0.9];

Numerics.tBackward = -20/p0Temp;
Numerics.tForward = 20/p0Temp;

Numerics.formulaOptions.relativeCutoff = 10^(-6);

WPN = packWPN(Wavepackets,Potentials,Numerics);

% WPN = Preprocess(WPN,OutputOptions);
% [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);
% psiHat = makePsiHatFull(Wavepackets.PsiHatUpCrossing,Wavepackets,Numerics);
% [x,p,xg,pg] = makeXP(Numerics);
% plot(p,abs(psiHat));

tic

[WPN,OutputOptions,RelError,MassError] = ExactVsFormula(WPN,OutputOptions);

toc

PlotExactFormulaCrossing1D(WPN)
pdfFile = [OutputOptions.DataDirO filesep 'SimpleNonGaussian.pdf'];
save2pdf(pdfFile,gcf);


