clear all

expList = {'ExpFormula1D','ExpLZD1D'};
nExp = length(expList);

for iExp = 1:nExp

    OutputOptions.PlotAnimation = 0;
    OutputOptions.Waitbar = 1;

    OutputOptions.DataSubDir = ['LZPaper' filesep 'Dual'];

    Numerics.compareAtCrossing = false;

    Numerics.epsilon = 1/50;
    Numerics.NumPoints = 2^14;

    Numerics.xStart = -40;
    Numerics.xEnd = 40;

    Numerics.d = 1;

    Potentials.V1 = 'V1LZDual';
    Potentials.V2 = 'V2LZDual';
    Potentials.V12 = 'V12LZDual';

    Potentials.Parameters.alpha = 0.5;
    Potentials.Parameters.delta = 0.5;
    Potentials.Parameters.x0 = 5;

    p0 = 5;
    Numerics.timestep = 1/100/p0;

    Numerics.formulaOptions.direction = 'down';

    Numerics.formulaOptions.ExpFunction = expList{iExp};

    if(strcmp(Numerics.formulaOptions.direction,'down'))
        Wavepackets.PsiHatUpCrossing = 'GaussianHat1D';
    else
        Wavepackets.PsiHatDownCrossing = 'GaussianHat1D';
    end    

    Wavepackets.Parameters.q0 = 0;
    Wavepackets.Parameters.p0 = p0;
    Numerics.tBackward = -30/p0;
    Numerics.tForward = 30/p0;

    Numerics.formulaOptions.relativeCutoff = 10^(-6);

    WPNIn = packWPN(Wavepackets,Potentials,Numerics);

    [WPN(iExp),OutputOptions,RelErr(iExp),RelAbsErr(iExp)] = ExactVsFormulaMultiple(WPNIn,OutputOptions);
end

PlotExactFormulaEnd1D(WPN(1))
PlotExactFormulaEnd1D(WPN(2))

pdfFile = [OutputOptions.DataDirO filesep 'LZDual.pdf'];

PlotDiabaticEigenvectors1D(WPN(1).Potentials,WPN(1).Numerics)

pdfFileD = [OutputOptions.DataDirO filesep 'LZDualPotentials.pdf'];
save2pdf(pdfFileD,gcf);