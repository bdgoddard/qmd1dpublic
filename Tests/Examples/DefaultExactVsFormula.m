% Default comparison between exact and formula

clear all;

Wavepackets = struct;
Potentials = struct;
Numerics = struct;

OutputOptions.Waitbar = 1;

Potentials.Parameters.alpha12 = 1;
%Potentials.Parameters.alpha12 = 0;
Potentials.Parameters.beta12 = 0.5;

Numerics.epsilon = 1/20;
Numerics.NumPoints = 2^11;

%Wavepackets.Parameters.x20 = 0.0;
Wavepackets.Parameters.x20 = 1;

% Numerics.epsilon = 1/30;
% Numerics.NumPoints = 2^11;

%OutputOptions.PlotAnimation = 1;

%Numerics.formulaOptions.tau = 'makeTau2DZero';

%Numerics.formula = 'FormulaLZ2DSlicing';
%Numerics.formulaOptions.LZ = 'LZ2DD';

%Numerics.formulaOptions.ExpFunction = 'ExpLZD2D';


Numerics.compareAtCrossing = true;

WPN = packWPN(Wavepackets,Potentials,Numerics);

[WPN,TotalError] = ExactVsFormula(WPN,OutputOptions);

[Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

