function [WPN,OutputOptions,RelativeError,RelativeMassError] = ExactVsFormula(WPN,OutputOptions)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%     Compares the exact dynamics with the formula result in    %%%%%%
%%%%%%     nD (currently restricted to a maximum of 2D)              %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if(nargin<1)
        WPN = struct;
    end
    if(nargin<2)
        OutputOptions = struct;
    end

    [WPN,OutputOptions] = Preprocess(WPN,OutputOptions);
    
    % Compute results of formula
    disp('Computing formula results ...');
    DataDirF = OutputOptions.DataDirF;
    WPNFormula = DataStorage(DataDirF,@makeFormula,WPN,OutputOptions,false);
    [WavepacketsF,~,NumericsF] = unpackWPN(WPNFormula);
    disp('Done.');
    
    % Compute results of exact dynamics
    disp('Computing exact results ...');
    DataDirE = OutputOptions.DataDirE;
    
    % remove formula options from numerics
    [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);
    NumericsE = Numerics;
    NumericsE.tCrossing = NumericsF.tCrossing;
    IgnoreList = {'formula','formulaOptions','RemainderFormula'};
    nIgnore = length(IgnoreList);
    for iIgnore = 1:nIgnore
        if(isfield(NumericsE,IgnoreList{iIgnore}))
            NumericsE = rmfield(NumericsE,IgnoreList{iIgnore});
        end
    end
    NumericsE.direction = Numerics.formulaOptions.direction;
    WPN = packWPN(Wavepackets,Potentials,NumericsE);
    WPNExact = DataStorage(DataDirE,@makeExact,WPN,OutputOptions,false);
    [WavepacketsE,~,~] = unpackWPN(WPNExact);
    disp('Done.');

    atCrossing = Numerics.compareAtCrossing;
    
    Wavepackets = WavepacketsE;
    
    % Compare to formula result
    
    direction = Numerics.formulaOptions.direction;
    
    if(atCrossing)
        Wavepackets.FormulaHatCrossing = WavepacketsF.FormulaHatCrossing;
        if(strcmp(direction,'down'))
            exact = Wavepackets.ExactHatDownCrossing;
        else
            exact = Wavepackets.ExactHatUpCrossing;
        end
        formula = Wavepackets.FormulaHatCrossing;
        
    else
        Wavepackets.FormulaHatFinal = WavepacketsF.FormulaHatFinal;
        if(strcmp(direction,'down'))
            exact = Wavepackets.ExactHatDownFinal;
        else
            exact = Wavepackets.ExactHatUpFinal;
        end
        formula = Wavepackets.FormulaHatFinal;
    end
      
    [RelativeError, RelativeAbsError, RelativeMassError] = makeError(formula,exact)
    
    Wavepackets = cleanWavepackets(Wavepackets);
    Numerics.tCrossing = NumericsF.tCrossing;
    WPN = packWPN(Wavepackets,Potentials,Numerics);
    
end