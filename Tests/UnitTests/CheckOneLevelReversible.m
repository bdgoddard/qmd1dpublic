clear all

% Set up numerics

Numerics.epsilon = (2000)^(-1/2);
Numerics.NumPoints = 2^13;

Numerics.xStart = -20;
Numerics.xEnd = 20;

Numerics.d = 1;

% Set up potentials

Potentials.V1 = 'V1Simple';
Potentials.V2 = 'V2Simple';
Potentials.V12 = 'V12Simple';

Potentials.Parameters.delta0 = 0.005;

% Set up wavepacket

p0 = 1;
Wavepackets.PsiHatDownInitial = 'GaussianHat1D';
Wavepackets.Parameters.q0 = -5;
Wavepackets.Parameters.p0 = p0;

% Set up times

Numerics.timestep = 1/100/p0;
Numerics.tBackward = 0/p0;
Numerics.tForward = 10/p0;

% Set output options

OutputOptions.PlotAnimation = 1;
OutputOptions.Waitbar = 1;

% Preprocess

WPN = packWPN(Wavepackets,Potentials,Numerics);
[WPN,OutputOptions] = Preprocess(WPN,OutputOptions);
[Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

% Construct initial wavepacket

Potentials.V = 'makeVL';
PsiHatFn = Wavepackets.PsiHatDownInitial;
PsiHatExact = makePsiHatFull(PsiHatFn,Wavepackets,Numerics);
Wavepackets.PsiHat = PsiHatExact;

% Do forwards evolution

Numerics.time = Numerics.tForward;
WPN = packWPN(Wavepackets,Potentials,Numerics);
WForward = ExactOneLevel(WPN,OutputOptions);
WForward.Psi = [];

% Do backwards evolution

Numerics.time = -Numerics.tForward;
WPN = packWPN(WForward,Potentials,Numerics);
WForwardBackward = ExactOneLevel(WPN,OutputOptions);

% Compute error

PsiHatApprox = WForwardBackward.PsiHat;
[RelativeError, RelativeAbsError] = makeError(PsiHatExact,PsiHatApprox)