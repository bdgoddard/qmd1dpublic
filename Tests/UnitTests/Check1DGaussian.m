clear all

Numerics.epsilon = 1/20;
Numerics.NumPoints = 2^12;

Numerics.xStart = -30;
Numerics.xEnd = 30;

Numerics.d = 1;

Numerics = NumericsStruct(Numerics);
Numerics = addXPNumerics(Numerics);

Wavepackets.epsilon = Numerics.epsilon;
Wavepackets.Parameters.p0 = 5;
Wavepackets.Parameters.q0 = 1;

[x,p,xg,pg] = makeXP(Numerics);

Gaussian = Gaussian1D(Wavepackets,Numerics.x);
GaussianHat = GaussianHat1D(Wavepackets,Numerics.p);

GaussianTest = eIFTn(GaussianHat,Numerics);
GaussianHatTest = eFTn(Gaussian,Numerics);

norm(Gaussian-GaussianTest)
norm(GaussianHat-GaussianHatTest)