function  Wavepackets = ExactTwoLevelWithFormula(WPN,OutputOptions)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the evolution of the wavepacket on a 2D potential V.  Output  %
% is two vectors in momentum space.  Also compute the instantaneous       %
% introduction and propagation of formula results for multiple avoided    %
% crossings.                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

%% Initialisation
% Numerics
time=Numerics.time; dt=Numerics.timestep;

[x,p,xg,pg] = makeXP(Numerics);
Numerics.x  = x;
Numerics.p  = p;
Numerics.xg = xg;
Numerics.pg = pg;

d         = Numerics.d; 
dx        = Numerics.xStep; 
dp        = Numerics.pStep;
epsilon   = Numerics.epsilon;
direction = Numerics.direction;

% Potentials
repGrid     = replicateGrid(xg,Numerics);
[V1,V2,V12] = makeV1V2V12(Potentials,repGrid{:});

% Potential for formula
VFFn = str2func(Potentials.VFormula);
VF   = VFFn(Potentials,repGrid{:});

% Avoided crossing data
%ACPsiHat        = Wavepackets.ACPsiHat;
temp   = load(Wavepackets.AC);
AC     = temp.Data;

ACPsiHatFormula = AC.ACPsiHatFormula;
%ACPsiHat        = AC.ACPsiHat;
ACTimesPos      = AC.ACTimesPos;
noAC            = AC.noAC;
nextAC          = 1;
nextACTimesPos  = ACTimesPos(1);

% Output options
Waitbar       = OutputOptions.Waitbar;
CheckEnergy   = OutputOptions.CheckTotalEnergy; 
CheckMass     = OutputOptions.CheckTotalMass;
CheckMomentum = OutputOptions.CheckTotalMomentum;
CentreofMass  = OutputOptions.CheckTotalCOM;
PlotAnimation = OutputOptions.PlotAnimation; 
SaveAnimation = OutputOptions.SaveAnimation;

if ( ( isfield(Wavepackets,'PsiUp') && ~isempty(Wavepackets.PsiUp) ) ...
        || ( isfield(Wavepackets,'PsiDown') && ~isempty(Wavepackets.PsiDown) ) )
    initialSpace = true;
else
    initialSpace = false;
end

% Initial conditions
if initialSpace % have to define PsiInitial or PsiHatInitial for one level dynamics
    
    if(ischar(Wavepackets.PsiUp))
        psiUp = makePsiFull(Wavepackets.PsiUp,Wavepackets,Numerics);
    else
        psiUp = Wavepackets.PsiUp;
    end
    
    if(ischar(Wavepackets.PsiDown))
        psiDown = makePsiFull(Wavepackets.PsiDown,Wavepackets,Numerics);
    else
        psiDown = Wavepackets.PsiDown;
    end

    psiHatUp   = eFTn(psiUp,Numerics);
    psiHatDown = eFTn(psiDown,Numerics);
else

    if(ischar(Wavepackets.PsiHatUp))
        psiHatUp = makePsiHatFull(Wavepackets.PsiHatUp,Wavepackets,Numerics);
    else
        if(~isempty(Wavepackets.PsiHatUp))
            psiHatUp = Wavepackets.PsiHatUp;
        else
            psiHatUp = zeros(size(pg));
        end
    end
   
    if(ischar(Wavepackets.PsiHatDown))
        psiHatDown = makePsiHatFull(Wavepackets.PsiHatDown,Wavepackets,Numerics);        
    else
        if(~isempty(Wavepackets.PsiHatDown))
            psiHatDown = Wavepackets.PsiHatDown;
        else
            psiHatDown = zeros(size(pg));
        end

    end
    psiUp      = eIFTn(psiHatUp,Numerics);
    psiDown    = eIFTn(psiHatDown,Numerics);
end

% Plotting bounds for 1D
if(PlotAnimation && d==1)
    
    absFormula = 0;
    
    for iAC = 1:noAC
        absFormula = absFormula + max(abs(ACPsiHatFormula(:,iAC)));
    end
    
    if(strcmp(direction,'down'))
        boundU = 2 * max(abs(psiHatUp));
        boundD = 2 * absFormula;
    else
        boundD = 2 * max(abs(psiHatDown));
        boundU = 2 * absFormula;
    end
end


%% Construct the KE and PE operators
% determine whether we're evolving forwards or backwards
% tDir is the sign change for the forwards in time eFT
if(time<0)
    tDir=-1;
    message={'Backwards Evolution on V'};
else
    tDir=1;
    message={'Forwards Evolution on V'};
end

% initializing KE and V evolution operators

% KE operators in p-space, half and full time step for Strang Splitting

kdiag1 = exp(-1i*tDir*dt*(sum(pg.^2,d+1))/(2*epsilon)/2); % half a timestep
kdiag2 = exp(-1i*tDir*dt*(sum(pg.^2,d+1))/(2*epsilon)); % full timestep
expVF  = exp(-1i*tDir*dt*VF/epsilon); % full timestep

[~,X,Z,~]=makeRhoXZTrace(Potentials,repGrid{:});

if(ischar(Numerics.ExpV))
    temp = load(Numerics.ExpV);
    u = temp.Data;
else
    u=Numerics.ExpV;
end

% to do: generalise to n dimensions
if d==2    
    % separate u into 4 m x n matrices to avoid a for loop in the dynamics
    aV  = squeeze(u(1,1,:,:));
    bV  = squeeze(u(1,2,:,:));
    cV  = squeeze(u(2,1,:,:));
    dV = squeeze(u(2,2,:,:));
elseif d==1
    % separate u into 4 m x n matrices to avoid a for loop in the dynamics
    aV  = squeeze(u(1,1,:));
    bV  = squeeze(u(1,2,:));
	cV  = squeeze(u(2,1,:));
    dV = squeeze(u(2,2,:));
else
    disp('Higher Dimensions not yet supported')
end

%% Move to the Diabatic regime using eigenfunctions

% deal with the case when X is close to zero
mask    = abs(X)<10^(-15);
maskPos = mask & (Z>=0);
maskNeg = mask & (Z<0);

% upper level
Phi1plus = (Z+sqrt(Z.^2+X.^2))./X;
% (Z + abs(Z))/X + O(X)
Phi1minus = ones(size(Phi1plus));

% deal with region where X is small
Phi1plus(maskPos)  = 1;
Phi1minus(maskPos) = 0;
Phi1plus(maskNeg)  = 0;
Phi1minus(maskNeg) = 1;

% normalization
k1        = 1./sqrt(Phi1plus.^2+Phi1minus.^2);
Phi1plus  = k1.*Phi1plus;
Phi1minus = k1.*Phi1minus;

% lower level
Phi2plus = (Z-sqrt(Z.^2+X.^2))./X;
% (Z - abs(Z))/X + O(X)
Phi2minus = ones(size(Phi2plus));

% deal with region where X is small
Phi2plus(maskPos)  = 0;
Phi2minus(maskPos) = 1;
Phi2plus(maskNeg)  = -1;
Phi2minus(maskNeg) = 0;

% normalization
k2        = 1./sqrt(Phi2plus.^2+Phi2minus.^2);
Phi2plus  = k2.*Phi2plus;
Phi2minus = k2.*Phi2minus;

% The diabatic wavefunctions
if(strcmp(direction,'down'))
    psiPlus  = psiUp.*Phi1plus;
    psiMinus = psiUp.*Phi1minus; % Assumes there is nothing on lower level.
else
    psiPlus  = psiDown.*Phi2plus;
    psiMinus = psiDown.*Phi2minus; % Assumes there is nothing on upper level.
end
    
psiHatPlus    = eFTn(psiPlus,Numerics);
psiHatMinus   = eFTn(psiMinus,Numerics);
psiHatFormula = zeros(size(psiHatPlus));

% Get number of time steps
steps = round(abs(time)/dt);
  
%% Initialize Checks
if CheckEnergy
    energyup   = zeros(1,steps);
    energydown = zeros(1,steps);
end
if CheckMass
    massup      = zeros(1,steps);
    massdown    = zeros(1,steps);
    massformula = zeros(1,steps);
end
if CheckMomentum
    momentumup   = zeros(1,steps);
    momentumdown = zeros(1,steps);
end
if CentreofMass
    CoMup      = zeros(steps,d);
    CoMdown    = zeros(steps,d);
    CoMformula = zeros(steps,d);
end

% Start the progress bar
if Waitbar
    wbe = waitbar(0,message{:});
end

%% Perform the Dynamics
% do half a timestep for the KE to enable faster Strang splitting
% ie we do half a KE, then N-1 full V / KE steps, then 1 full V then a
% final half KE.  This reduces the number of Fourier transforms necessary.

% do initial half timestep for KE on both levels

psiHatPlus    = kdiag1.*psiHatPlus;
psiHatMinus   = kdiag1.*psiHatMinus;
psiHatFormula = kdiag1.*psiHatFormula;

% transform to position space

psiPlus    = eIFTn(psiHatPlus,Numerics);
psiMinus   = eIFTn(psiHatMinus,Numerics);
psiFormula = eIFTn(psiHatFormula,Numerics);

% Set up the animation file
if SaveAnimation
    h = figure('Name','Exact calculation of wavepacket evolution');
    filename = 'testTwoDExactTwoLevel.gif';
end
% Do full time steps
for t=1:steps
    % Advance the progress bar
    if Waitbar
        waitbar( t/steps, wbe, message{:});
    end
        
    % V operator time step
    psiPlusTemp = aV.*psiPlus+bV.*psiMinus;
    psiMinus    = cV.*psiPlus+dV.*psiMinus;
    psiPlus     = psiPlusTemp;
    psiFormula  = expVF.*psiFormula;
    
    % transform to momentum space
    psiHatPlus    = eFTn(psiPlus,Numerics);
    psiHatMinus   = eFTn(psiMinus,Numerics);
    psiHatFormula = eFTn(psiFormula,Numerics);
    
    if(t == steps)
        psiHatPlus    = kdiag1.*psiHatPlus;
        psiHatMinus   = kdiag1.*psiHatMinus;
        psiHatFormula = kdiag1.*psiHatFormula;
    else
        psiHatPlus    = kdiag2.*psiHatPlus;
        psiHatMinus   = kdiag2.*psiHatMinus;
        psiHatFormula = kdiag2.*psiHatFormula;
    end

    if(t == nextACTimesPos)
        
        disp('Adding formula wavepacket');
        
        % KE back half a time step to correct time
        psiHatFormula = psiHatFormula./kdiag1;
        
        toAdd = ACPsiHatFormula(:,nextAC);
        
        thisMomSign = sign( sum(p.*abs(toAdd).^2) );

        prefactor = 1;
        if(nextAC>1 && thisMomSign == lastMomSign)
            prefactor = -1;
        end
        lastMomSign = thisMomSign;
        
        % add in next formula wavepacket
        
        psiHatFormula = psiHatFormula + prefactor * toAdd;
                
        % put back in sync with two-level calculation
        psiHatFormula = kdiag1.*psiHatFormula;
        
        % set next AC
        nextAC = nextAC + 1;
        if(nextAC <= noAC)
            nextACTimesPos = ACTimesPos(nextAC);
        else
            nextACTimesPos = NaN;
        end
        
                      
    end

    % transform to position space
    psiPlus    = eIFTn(psiHatPlus,Numerics);
    psiMinus   = eIFTn(psiHatMinus,Numerics); 
    psiFormula = eIFTn(psiHatFormula,Numerics); 
    
%%%%%%%%%%%%%%%%%%%%%%%         Checks          %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Use the adiabatic representation to run checks 
    if CheckEnergy || CheckMass || CheckMomentum || CentreofMass || PlotAnimation 
        psiUp      = Phi1plus.*psiPlus+Phi1minus.*psiMinus;
        psiDown    = Phi2plus.*psiPlus+Phi2minus.*psiMinus;
        psiHatUp   = eFTn(psiUp,Numerics);
        psiHatDown = eFTn(psiDown,Numerics);
    end

    if CheckEnergy   
        Z1up   = (abs(psiUp).^2).*V1+(abs(psiDown).^2).*V12; % PE up
        Z2up   = (abs(psiHatUp).^2).*(sum(pg.^2,d+1)/2); % KE up
        Z1down = (abs(psiUp).^2).*V12+(abs(psiDown).^2).*V2; % PE down
        Z2down = (abs(psiHatDown).^2).*(sum(pg.^2,d+1)/2); % KE down

        energyup(t)   = dx^d*sum(Z1up(:))+dp^d*sum(Z2up(:)); % Total up
        energydown(t) = dx^d*sum(Z1down(:))+dp^d*sum(Z2down(:)); % Total down
    end
    if CheckMass
        massup(t)   = dx^d*sum(abs(psiUp(:)).^2);
        massdown(t) = dx^d*sum(abs(psiDown(:)).^2);
        massformula(t) = dx^d*sum(abs(psiFormula(:)).^2);
    end
    if CheckMomentum
        momentumup(t)   = dp^d*sum(abs(psiHatUp(:)).^2);
        momentumdown(t) = dp^d*sum(abs(psiHatDown(:)).^2);
    end
    if CentreofMass
        Mup   = dx^d*sum(abs(psiUp(:)).^2);
        Mdown = dx^d*sum(abs(psiDown(:)).^2);
        Mformula = dx^d*sum(abs(psiFormula(:)).^2);
        for dim=1:d
            integrandup    = abs(psiUp).^2.*repGrid{dim};
            CoMup(t,dim)   = 1/Mup.*dx^d*sum(integrandup(:));
            integranddown  = abs(psiDown).^2.*repGrid{dim};
            CoMdown(t,dim) = 1/Mdown.*dx^d*sum(integranddown(:));
            integrandformula  = abs(psiFormula).^2.*repGrid{dim};
            CoMformula(t,dim) = 1/Mformula.*dx^d*sum(integrandformula(:));
        end
        
    end
%%%%%%%%%%%%%%%    Save and Plot Animations         %%%%%%%%%%%%%%%%%%%%%%%%  
    
    if PlotAnimation
        if d==2
            figure(1)
            subplot(2,1,1)
    %         fig1=pcolor(x(1,:),x(2,:),abs(psiUp));
    %         set(fig1, 'EdgeColor', 'none');
            mesh(xg(:,:,1),xg(:,:,2),abs(psiUp))
            axis([-20 20 -20 20 0 1])
            subplot(2,1,2)
            mesh(xg(:,:,1),xg(:,:,2),abs(psiDown))
            axis([-20 20 -20 20 0 1])
            drawnow
        elseif d==1
            figure(1)
            subplot(2,1,1)
    %         fig1=pcolor(x(1,:),x(2,:),abs(psiUp));
    %         set(fig1, 'EdgeColor', 'none');
            plot(xg,abs(psiUp))
            %axis([-20 20 0 2])
            %axis([-20 20 0 boundU])
            ylim([0 boundU])
            subplot(2,1,2)
            plot(xg,abs(psiDown))
%            axis([-20 20 0 2])
%             axis([-20 20 0 boundD])
            ylim([0 boundD])
            
            if(strcmp(direction,'down'))
                subplot(2,1,2)
                hold on
                plot(xg,abs(psiFormula),'--r');
                hold off
            else
                subplot(2,1,1)
                hold on
                plot(xg,abs(psiFormula),'--r');
                hold off
            end
            drawnow
        end

        if SaveAnimation
            frame   = getframe(h);
            im      = frame2im(frame);
            [A,map] = rgb2ind(im,256); 
            if t == 1
                imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',0);
            elseif mod(t,5)==0
                imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',0);
            end
        end
    end
end

psiUp         = Phi1plus.*psiPlus + Phi1minus.*psiMinus;
psiDown       = Phi2plus.*psiPlus + Phi2minus.*psiMinus;
psiHatUp      = eFTn(psiUp,Numerics);
psiHatDown    = eFTn(psiDown,Numerics);
psiHatFormula = eFTn(psiFormula,Numerics);

% Close the progress bar
if(exist('wbe','var'))
    close(wbe);
end

%% Save results in Wavepacket struct
Wavepackets.PsiUp         = psiUp;
Wavepackets.PsiDown       = psiDown;
Wavepackets.PsiHatUp      = psiHatUp;
Wavepackets.PsiHatDown    = psiHatDown;
Wavepackets.PsiHatFormula = psiHatFormula;

%% Save Checks in Wavepacket struct
if CheckEnergy
    Wavepackets.EnergyUp   = energyup;
    Wavepackets.EnergyDown = energydown;
end
if CheckMass
    Wavepackets.MassUp      = massup;
    Wavepackets.MassDown    = massdown;
    Wavepackets.MassFormula = massformula;
end
if CheckMomentum
    Wavepackets.MomentumUp   = momentumup;
    Wavepackets.MomentumDown = momentumdown;
end
if CentreofMass
    CoMupSep                 = mat2cell(CoMup,steps,ones(d,1));
    Wavepackets.CoMup        = [CoMup,makeVU(Potentials,CoMupSep{:})];
    CoMdownSep               = mat2cell(CoMdown,steps,ones(d,1));
    Wavepackets.CoMdown      = [CoMdown,makeVL(Potentials,CoMdownSep{:})];
    CoMformulaSep            = mat2cell(CoMformula,steps,ones(d,1));
    Wavepackets.CoMformula   = [CoMformula,makeVU(Potentials,CoMformulaSep{:})];
end

end