function Wavepackets = ExactOneLeveltoAC(WPN,OutputOptions)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%     Evolve a wavepacket on a nD Potential V until it reaches a      %%%
%%%     local minimum.                                                  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

[x,p,xg,pg] = makeXP(Numerics);
Numerics.x = x;
Numerics.p = p;
Numerics.xg = xg;
Numerics.pg = pg;

% Numerics
time=Numerics.time; dt=Numerics.timestep;
d=Numerics.d; dx=Numerics.xStep; dp=Numerics.pStep;

% Potentials
repGrid = replicateGrid(xg,Numerics);
Vfunc = str2func(Potentials.V);

V=Vfunc(Potentials,repGrid{:});
epsilon=Numerics.epsilon;

% Output options
Waitbar=OutputOptions.Waitbar;

% Initial conditions
if ~isempty(Wavepackets.Psi) % have to define PsiInitial or PsiHatInitial for one level dynamics
    psi=Wavepackets.Psi;
    psiHat=eFTn(psi,Numerics);
else
    psiHat=Wavepackets.PsiHat;
    psi=eIFTn(psiHat,Numerics);
end

% determine whether we're evolving forwards or backwards
% tDir is the sign change for the forwards in time eFT
if(time<0)
    tDir=-1;
    message={'Backwards Evolution on V'};
else
    tDir=1;
    message={'Forwards Evolution on V'};
end

% KE and V evolution operators
kdiag=exp(-1i*tDir*dt*(sum(pg.^2,d+1))/(2*epsilon)/2); %half a timestep
expV=exp(-1i*tDir*dt*V/epsilon); % full timestep

%get number of steps
steps=round(abs(time)/dt);

% Begin the waitbar    
if Waitbar==1
    wbe=waitbar(0,message{:});
end

% Determine initial centre of mass on rho
CoM=zeros(d,1);
M=dx^d*sum(abs(psi(:)).^2);
for dim=1:d
    integrand=abs(psi).^2.*repGrid{dim};
    CoM(dim)=1/M.*dx^d*sum(integrand(:));
end
CoMSep=num2cell(CoM);
rhoCoM=makeRho(Potentials,CoMSep{:});

%% Perform the dynamics
% Enable a faster Strang splitting by doing a half timestep first, then
% whole timesteps, until the final step, where we do half again.

% transform to position space
psi=eIFTn(psiHat,Numerics);

rhoCoMprevious=rhoCoM;
for t=1:steps
    % Advance waitbar
    if Waitbar==1
        waitbar( t/steps, wbe, message{:});
    end  

    % half timestep for KE
    psiHat=kdiag.*psiHat;

    % transform to position space
    psi=eIFTn(psiHat,Numerics);

    % full timestep V
    psi=expV.*psi;
    
%     if(d==1)
%         plot(x,abs(psi))
%         pause(0.01)
%     end
    
    % transform to momentum space
    psiHat=eFTn(psi,Numerics);  
    
    % half timestep KE
    psiHat=kdiag.*psiHat;
      
    % transform to position space
    psi=eIFTn(psiHat,Numerics);

    % find centre of mass on rho
    M=dx^d*sum(abs(psi(:)).^2);
    for dim=1:d
        integrand=abs(psi).^2.*repGrid{dim};
        CoM(dim)=1/M.*dx^d*sum(integrand(:));
    end
    CoMSep=num2cell(CoM);
    rhoCoM=makeRho(Potentials,CoMSep{:});
    
    if rhoCoM>rhoCoMprevious        
        % minimum was at previous timestep
        ACtime=tDir*(t-1)*dt;

        % reverse dynamics by one timestep
        psiHat=psiHat./kdiag;
        psi=eIFTn(psiHat,Numerics);
        psi=psi./expV;
        psiHat=eFTn(psi,Numerics);
        psiHat=psiHat./kdiag;
        psi=eIFTn(psiHat,Numerics);

        M=dx^d*sum(abs(psi(:)).^2);
        for dim=1:d
            integrand=abs(psi).^2.*repGrid{dim};
            CoM(dim)=1/M.*dx^d*sum(integrand(:));
        end
        rhoCoM=rhoCoMprevious;
        break
    else
        rhoCoMprevious=rhoCoM;
    end
    
    % wavepacket does not reach minimum by predicted time
    if t==steps
        % minimum time at 0
        ACtime=0;
        % Return original wavepackets
        if ~isempty(Wavepackets.Psi)
            psi=Wavepackets.Psi;
            psiHat=eFTn(psi,Numerics);
        else
            psiHat=Wavepackets.PsiHat;
            psi=eIFTn(psiHat,Numerics);
        end
    end
end

if Waitbar==1
    close(wbe);
end

Wavepackets.Psi=psi;
Wavepackets.PsiHat=psiHat;
Wavepackets.ACtime=ACtime;
Wavepackets.ACCoM=[CoM;rhoCoM];
end