function WPN = makeExact(WPN,OutputOptions)

    [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);
    
    direction = Numerics.direction;
    
    %% Do one-level dynamics to compute initial condition
    disp('Computing initial wavepacket ...');
    % Potential surface is the one we start on
    if(strcmp(direction,'down'))
        Potentials.V = 'makeVU';
        psiHatFn = Wavepackets.PsiHatUpCrossing;
    else
        Potentials.V = 'makeVL';
        psiHatFn = Wavepackets.PsiHatDownCrossing;
    end
%    Potentials.V = 'makeVU';
    Numerics.time = Numerics.tBackward;

    % Construct wavepacket at crossing point
    Wavepackets.PsiHat = makePsiHatFull(psiHatFn,Wavepackets,Numerics);
    
    
    
    Wavepackets.Psi = [];
    
    % Do one-level dynamics
    WPN = packWPN(Wavepackets,Potentials,Numerics);
    Wavepackets = ExactOneLevel(WPN,OutputOptions);
    Wavepackets.PsiHatInitial = Wavepackets.PsiHat;
    
    % clean up
    Wavepackets.Psi = [];
    Wavepackets.PsiHat = [];
    Potentials.V = [];
    disp('Done.');

    %% Do 2-level dynamics forward in time until far from crossing
    
    % Set initial conditions for two level calculations
    Numerics.time = Numerics.tTotal;

    % Construct the potential operator
    disp('Computing potential operator ...');
    DataDirV = OutputOptions.DataDirV;
    PN.Potentials = Potentials;
    NumericsV = Numerics;
    NumericsV.tDir = sign(NumericsV.time);
    IgnoreList = {'tBackward','tForward','time','compareAtCrossing','direction'};
    nIgnore = length(IgnoreList);
    for iIgnore = 1:nIgnore
        if(isfield(NumericsV,IgnoreList{iIgnore}))
            NumericsV = rmfield(NumericsV,IgnoreList{iIgnore});
        end
    end
    PN.Numerics = NumericsV;

    Numerics.ExpV = DataStorage(DataDirV,@makeExpV,PN,OutputOptions);
    disp('Done.');
    
    % Do two-level evolution
    disp('Computing exact 2-level dynamics ...');
    
    if(strcmp(direction,'down'))
        Wavepackets.PsiHatUp = Wavepackets.PsiHatInitial;
        Wavepackets.PsiHatDown = zeros(size(Wavepackets.PsiHatUp));
    else
        Wavepackets.PsiHatDown = Wavepackets.PsiHatInitial;
        Wavepackets.PsiHatUp = zeros(size(Wavepackets.PsiHatDown));
    end
        
    Wavepackets.PsiUp = []; 
    Wavepackets.PsiDown = [];
    
    WPN = packWPN(Wavepackets,Potentials,Numerics);
    Wavepackets = ExactTwoLevel(WPN,OutputOptions);

    Wavepackets.ExactHatUpFinal = Wavepackets.PsiHatUp;
    Wavepackets.ExactHatDownFinal = Wavepackets.PsiHatDown;

    %% Evolve back to crossing
    
    if(Numerics.compareAtCrossing)
        disp('Evolving back to crossing ...');
        Numerics.time = - Numerics.tForward;
        
        % Evolve lower level back
        Potentials.V = 'makeVL';
        Wavepackets.PsiHat = Wavepackets.ExactHatDownFinal;
        Wavepackets.Psi = [];
        WPN = packWPN(Wavepackets,Potentials,Numerics);
        Wavepackets = ExactOneLevel(WPN,OutputOptions);
        Wavepackets.ExactHatDownCrossing = Wavepackets.PsiHat;
          
        % Evolve upper level back
        Potentials.V = 'makeVU';
        Wavepackets.PsiHat = Wavepackets.ExactHatUpFinal;
        Wavepackets.Psi = [];
        WPN = packWPN(Wavepackets,Potentials,Numerics);
        Wavepackets = ExactOneLevel(WPN,OutputOptions);
        Wavepackets.ExactHatUpCrossing = Wavepackets.PsiHat;
    end
    
    %% clean up
    Wavepackets.PsiUp = [];
    Wavepackets.PsiDown = [];
    Wavepackets.PsiHatUp = [];
    Wavepackets.PsiHatDown = [];
    Numerics.ExpV = [];

    WPN = packWPN(Wavepackets,Potentials,Numerics);
    disp('Done.');
end

