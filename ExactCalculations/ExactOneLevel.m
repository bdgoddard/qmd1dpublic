function Wavepackets=ExactOneLevel(WPN,OutputOptions)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%     Calculate the evolution of a wavepacket on a nD Potential V     %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

[x,p,xg,pg] = makeXP(Numerics);
Numerics.x = x;
Numerics.p = p;
Numerics.xg = xg;
Numerics.pg = pg;




% Numerics
time=Numerics.time; dt=Numerics.timestep;
d=Numerics.d; dx=Numerics.xStep; dp=Numerics.pStep;

if(d==2)
    x1 = x(:,1);
    x2 = x(:,2);
end

% Potentials
repGrid = replicateGrid(xg,Numerics);
Vfunc = str2func(Potentials.V);

V=Vfunc(Potentials,repGrid{:});
epsilon=Numerics.epsilon;

% Output options
Waitbar=OutputOptions.Waitbar;
CheckEnergy=OutputOptions.CheckTotalEnergy; 
CheckMass=OutputOptions.CheckTotalMass;
CheckMomentum=OutputOptions.CheckTotalMomentum;
CentreofMass=OutputOptions.CheckTotalCOM;
PlotAnimation=OutputOptions.PlotAnimation;
SaveAnimation=OutputOptions.SaveAnimation;

% Initial conditions
if (isfield(Wavepackets,'Psi') && ~isempty(Wavepackets.Psi) )% have to define PsiInitial or PsiHatInitial for one level dynamics
    if(ischar(Wavepackets.Psi))
        psi = makePsiFull(Wavepackets.Psi,Wavepackets,Numerics);
    else
        psi = Wavepackets.Psi;
    end
    psiHat=eFTn(psi,Numerics);
else
    if(ischar(Wavepackets.PsiHat))
        psiHat = makePsiHatFull(Wavepackets.PsiHat,Wavepackets,Numerics);
    else
        psiHat = Wavepackets.PsiHat;
    end
    psi=eIFTn(psiHat,Numerics);
end


% determine whether we're evolving forwards or backwards
% tDir is the sign change for the forwards in time eFT
if(time<0)
    tDir=-1;
    message={'Backwards Evolution on V'};
else
    tDir=1;
    message={'Forwards Evolution on V'};
end

% KE and V evolution operators
kdiag1=exp(-1i*tDir*dt*(sum(pg.^2,d+1))/(2*epsilon)/2); %half a timestep
kdiag2=exp(-1i*tDir*dt*(sum(pg.^2,d+1))/(2*epsilon)); %full timestep

expV=exp(-1i*tDir*dt*V/epsilon); % full timestep

%get number of steps
steps=round(abs(time)/dt);

%%%%%%%%%%%%%%%%%%%%%          Checks          %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize the energy, mass and centre of mass checks
if CheckEnergy==1
    energy=zeros(steps,1);
end
if CheckMass==1
    mass=zeros(steps,1);
end
if CheckMass==1
    momentum=zeros(steps,1);
end
if CentreofMass==1
    CoM=zeros(steps,d);
end

% Begin the waitbar    
if Waitbar==1
    wbe=waitbar(0,message{:});
end

%% Perform the dynamics
% Enable a faster Strang splitting by doing a half timestep first, then
% whole timesteps, until the final step, where we do half again.

% do initial half timestep for KE
psiHat=kdiag1.*psiHat;

% transform to position space
psi=eIFTn(psiHat,Numerics);

%%%%%%%%%%%%%%%%%          Saving Animation          %%%%%%%%%%%%%%%%%%%%%%
if SaveAnimation==1
    figure('name', 'One level evolution of wavepacket')
    filename = 'ExactOneLevel.gif';
end

for t=1:steps
    % Advance waitbar
    if Waitbar==1
        waitbar( abs(t)/steps, wbe, message{:});
    end  
    
    % full timestep V
    psi=expV.*psi;
    
    % transform to momentum space
    psiHat=eFTn(psi,Numerics);  
    if(t<steps)
        % full timestep KE#
        psiHat=kdiag2.*psiHat;
    else
        % half timestep KE
        psiHat=kdiag1.*psiHat;
    end
  
    % transform to position space
    psi=eIFTn(psiHat,Numerics);
%%%%%%%%%%%%%%%%%%%%%           Checks           %%%%%%%%%%%%%%%%%%%%%%%%%%
    if CheckEnergy==1
    % Check the energy of the system at time t
        Z1=(abs(psi).^2).*V; % PE
        Z2=(abs(psiHat).^2).*((sum(pg.^2,d+1))/2); % KE
        energy(t)=dx^d*sum(Z1(:))+dp^d*sum(Z2(:)); % Total
    end
    if CheckMass==1
        mass(t)=dx^d*sum(abs(psi(:)).^2);
    end
    if CheckMomentum==1
        momentum(t)=dp^d*sum(abs(psiHat(:)).^2);
    end

    if CentreofMass==1
        M=dx^d*sum(abs(psi(:)).^2);
        for dim=1:d
            integrand=abs(psi).^2.*repGrid{dim};
            CoM(t,dim)=1/M.*dx^d*sum(integrand(:));
        end
    end
    
%%%%%%%%%%%%          Plotting and Saving Animation          %%%%%%%%%%%%%%
    if PlotAnimation==1
        if d==2
            h = pcolor(x1,x2,abs(psi));
            set(h, 'EdgeColor', 'none');
            drawnow
        elseif d==1
            plot(x,abs(psi))
            drawnow
        else
            message('I cannot plot in higher dimensions');
        end
        if SaveAnimation==1
            if d==2 || d==1
                frame = getframe;
                im = frame2im(frame);
                [A,map] = rgb2ind(im,256); 
                if t == 1
                    imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',0);
                elseif mod(t,2)==0
                    imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',0);
                end   
            end
        end
        
    end
    
end
if Waitbar==1
    close(wbe);
end

Wavepackets.Psi=psi;
Wavepackets.PsiHat=psiHat;

%%%%%%%%%%%%%%%%%%%%%%%%%            Checks %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save energy checks etc in Wavepacket struct
if CheckEnergy==1
    Wavepackets.EnergyOneLevel=energy;
end
if CheckMass==1
    Wavepackets.MassOneLevel=mass;
end
if CheckMomentum==1
    Wavepackets.MomentumOneLevel=momentum;
end
if CentreofMass==1
    CoMSep=mat2cell(CoM,steps,ones(d,1));
    Wavepackets.CoMOneLevel=[CoM,Vfunc(Potentials,CoMSep{:})];
end

end