function WavepacketsAC = DetectAvoidedCrossings(WPN,OutputOptions)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%     Evolve a wavepacket on a nD Potential V until it reaches a      %%%
%%%     local minimum, store the time and position, and continue until  %%%
%%%     time.                                                           %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


PlotAnimation = OutputOptions.PlotAnimation;

[Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

[x,p,xg,pg] = makeXP(Numerics);
Numerics.x = x;
Numerics.p = p;
Numerics.xg = xg;
Numerics.pg = pg;

% Numerics
time=Numerics.time; 
dt=Numerics.timestep;
d=Numerics.d; 
dx=Numerics.xStep; 
dp=Numerics.pStep;

% Formula function
formulaFn = str2func(Numerics.formula);
direction = Numerics.formulaOptions.direction;

% Potentials
repGrid = replicateGrid(xg,Numerics);
Vfunc = str2func(Potentials.V);

V=Vfunc(Potentials,repGrid{:});
epsilon=Numerics.epsilon;

rho = makeRho(Potentials,repGrid{:});

% Output options
Waitbar=OutputOptions.Waitbar;

% Initial conditions
if (isfield(Wavepackets,'Psi') && ~isempty(Wavepackets.Psi) )% have to define PsiInitial or PsiHatInitial for one level dynamics
    if(ischar(Wavepackets.Psi))
        psi = makePsiFull(Wavepackets.Psi,Wavepackets,Numerics);
    else
        psi = Wavepackets.Psi;
    end
    psiHat=eFTn(psi,Numerics);
else
    if(ischar(Wavepackets.PsiHat))
        psiHat = makePsiHatFull(Wavepackets.PsiHat,Wavepackets,Numerics);
    else
        psiHat = Wavepackets.PsiHat;
    end
    psi=eIFTn(psiHat,Numerics);
end


% determine whether we're evolving forwards or backwards
% tDir is the sign change for the forwards in time eFT
if(time<0)
    tDir=-1;
    message={'Backwards Evolution on V'};
else
    tDir=1;
    message={'Forwards Evolution on V'};
end

% KE and V evolution operators
kdiag=exp(-1i*tDir*dt*(sum(pg.^2,d+1))/(2*epsilon)/2); %half a timestep
expV=exp(-1i*tDir*dt*V/epsilon); % full timestep

%get number of steps
steps=round(abs(time)/dt);

% Begin the waitbar    
if Waitbar==1
    wbe=waitbar(0,message{:});
end

% find initial CoM
CoM = getCoM(psi);
CoMSep=num2cell(CoM);
rhoCoM=makeRho(Potentials,CoMSep{:});

%% Perform the dynamics

noAC              = 0;
ACTimes           = [];
ACTimesPos        = [];
ACCoM             = zeros(d,1);
ACPsiHat          = zeros(size(psiHat));
ACPsiHatFormula   = zeros(size(psiHat));

WavepacketsF = Wavepackets;

rhoCoM = zeros(steps,1);

for t=1:steps
    % Advance waitbar
    if Waitbar==1
        waitbar( t/steps, wbe, message{:});
    end  

    % half timestep for KE
    psiHat=kdiag.*psiHat;

    % transform to position space
    psi=eIFTn(psiHat,Numerics);

    % full timestep V
    psi=expV.*psi;
    
    % transform to momentum space
    psiHat=eFTn(psi,Numerics);  
    
    % half timestep KE
    psiHat=kdiag.*psiHat;
      
    % transform to position space
    psi=eIFTn(psiHat,Numerics);

    % find centre of mass on rho
    CoM = getCoM(psi);
    
    CoMSep = num2cell(CoM);
    rhoCoM(t) = makeRho(Potentials,CoMSep{:});
    
    % detect avoided crossing at previous time step
    if t>2 && (rhoCoM(t-2) > rhoCoM(t-1)) && (rhoCoM(t) > rhoCoM(t-1)) ...
             &&  (rhoCoM(t) - rhoCoM(t-1) > 10^(-12))
        
        noAC = noAC + 1;
        
        % minimum was at previous timestep
        ACTimes(noAC)    = tDir*(t-1)*dt;  %#ok
        ACTimesPos(noAC) = t-1; %#ok

        psiHatAC = psiHatm1;
        psiAC    = eIFTn(psiHatAC,Numerics);
       
        % apply formula
        ACCoM(:,noAC) = getCoM(psiAC(:));
        if(strcmp(direction,'down'))
            WavepacketsF.PsiHatUpCrossing = psiHatAC;
        else
            WavepacketsF.PsiHatDownCrossing = psiHatAC;
        end
        
        WavepacketsF.CoM = ACCoM(:,noAC);
        WPNF = packWPN(WavepacketsF,Potentials,Numerics);
        
        psiHatACFormula = formulaFn(WPNF,OutputOptions);
                
        if PlotAnimation
            subplot(3,1,3)
            plot(p,abs(psiHatACFormula));
            hold all
        end
        
        ACPsiHat(:,noAC) = psiHatAC;
        ACPsiHatFormula(:,noAC) = psiHatACFormula;
        
        disp(['Avoided crossing detected at ' num2str(ACCoM(:,noAC))]);
    end
    
    % prepare for next time step
    psiHatm1 = psiHat;
    
    % plot wavepacket
    if PlotAnimation
        if d==2
            h = pcolor(x1,x2,abs(psi));
            set(h, 'EdgeColor', 'none');
            drawnow
        elseif d==1
            subplot(3,1,1)
            plot(x,abs(psi));
            subplot(3,1,2)
            plot(x,rho);
        else
            message('I cannot plot in higher dimensions');
        end
    end
end

% wavepacket does not reach minimum by predicted time
if noAC == 0
    % Return empty wavepackets and time
    ACTimes         = [];
    ACTimesPos      = [];
    ACCoM           = [];
    ACPsiHat        = [];
    ACPsiHatFormula = [];
end

if Waitbar==1
    close(wbe);
end

%Wavepackets.Psi=psi;
WavepacketsAC.ACPsiHat           = ACPsiHat;
WavepacketsAC.ACPsiHatFormula    = ACPsiHatFormula;
WavepacketsAC.ACTimes            = ACTimes;
WavepacketsAC.ACTimesPos         = ACTimesPos;
WavepacketsAC.ACCoM              = ACCoM;
WavepacketsAC.noAC               = noAC;
WavepacketsAC.rhoCoM             = rhoCoM;
%WavepacketsAC.PsiHat             = psiHat;

    function CoM = getCoM(psi)
        M=dx^d*sum(abs(psi(:)).^2);
        CoM = zeros(d,1);
        for dim=1:d
            integrand = abs(psi).^2.*repGrid{dim};
            CoM(dim)  = 1/M.*dx^d*sum(integrand(:));
        end
    end

end