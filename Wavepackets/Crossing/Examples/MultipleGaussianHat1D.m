function psiHat = MultipleGaussianHat1D(Wavepackets,varargin)

    %epsilon = Wavepackets.epsilon;
    q0List = Wavepackets.Parameters.q0;
    p0List = Wavepackets.Parameters.p0;
    wList  = Wavepackets.Parameters.w;
    
    nG = length(q0List);
    
    p = varargin{:};
    psiHat = zeros(size(p));
    
    WavepacketsG = Wavepackets;
    WavepacketsG = rmfield(WavepacketsG.Parameters,'w');
    for iG = 1:nG
        WavepacketsG.Parameters.q0 = q0List(iG);
        WavepacketsG.Parameters.p0 = p0List(iG);
     
        psiHatG = GaussianHat1D(WavepacketsG,varargin{:});
        psiHat = psiHat + wList(iG)*psiHatG;
    end
    
end