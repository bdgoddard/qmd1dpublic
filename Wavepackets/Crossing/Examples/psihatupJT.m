function psihatup = psihatupJT(Wavepackets,varargin)
% The wavefunction at the crossing point, in the adiabatic regime

    epsilon = Wavepackets.epsilon;      
    
    if(isfield(Wavepackets.Parameters,'p0'))
        p0 = Wavepackets.Parameters.p0;
    else
        p0=5;
    end
    
    psihatup=exp(-(varargin{1}-p0).^2/(2*epsilon));
end

