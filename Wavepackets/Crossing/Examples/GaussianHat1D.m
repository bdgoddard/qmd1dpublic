function psiHat = GaussianHat1D(Wavepackets,varargin)
% The wavefunction at the crossing point, in the adiabatic regime

    epsilon = Wavepackets.epsilon;
    q0 = Wavepackets.Parameters.q0;
    p0 = Wavepackets.Parameters.p0;
    
    p = varargin{1};

    psiHat = (pi*epsilon)^(-1/4) * exp(-1i * p0 * q0/epsilon) * ...
             exp( -(p-p0).^2/(2*epsilon) - 1i*q0*(p-p0)/epsilon );
end


