function psi = Gaussian1D(Wavepackets,varargin)
% The wavefunction at the crossing point, in the adiabatic regime

    epsilon = Wavepackets.epsilon;
    q0 = Wavepackets.Parameters.q0;
    p0 = Wavepackets.Parameters.p0;
    
    q = varargin{1};
    
    psi = (pi*epsilon)^(-1/4) * exp( -(q-q0).^2/(2*epsilon) + 1i*p0*(q-q0)/epsilon );
end


