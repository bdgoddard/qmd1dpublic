function WPN = makeFormula(WPN,OutputOptions)

    [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);
    direction = Numerics.formulaOptions.direction;
    
    formulaFn = str2func(Numerics.formula);
    
    if(Wavepackets.givenInitial)

        WavepacketsI = makePsiHatInitial(Wavepackets,Numerics);
        WavepacketsI.PsiHat = WavepacketsI.PsiHatInitial;
        WavepacketsI.Psi = [];
        
        NumericsI = Numerics;
        NumericsI.time = NumericsI.tTotal;
        
        PotentialsI = Potentials;
        if(strcmp(direction,'down'))
            PotentialsI.V = 'makeVU';
        else
            PotentialsI.V = 'makeVL';
        end
        
        WPNInitial = packWPN(WavepacketsI,PotentialsI,NumericsI);
        WCrossing  = ExactOneLeveltoAC(WPNInitial,OutputOptions);
        
        Numerics.tCrossing = WCrossing.ACtime;
        
        if(strcmp(direction,'down'))
            Wavepackets.PsiHatUpCrossing = WCrossing.PsiHat;
        else
            Wavepackets.PsiHatDownCrossing = WCrossing.PsiHat;
        end
        
    else
        
        Numerics.tCrossing = -Numerics.tBackward; 
    end
        
    WPN = packWPN(Wavepackets,Potentials,Numerics);
    
    % Apply formula    
    Wavepackets.FormulaHatCrossing = formulaFn(WPN,OutputOptions);

    if(~Numerics.compareAtCrossing)
        % Evolve formula wavepacket on transmitted level away from avoided crossing
        if(strcmp(direction,'down'))
            Potentials.V = 'makeVL';
        else
            Potentials.V = 'makeVU';
        end
        Wavepackets.PsiHat = Wavepackets.FormulaHatCrossing;
        Wavepackets.Psi = [];
        Numerics.time = Numerics.tForward - Numerics.tCrossing;

        WPN = packWPN(Wavepackets,Potentials,Numerics);

        Wavepackets = ExactOneLevel(WPN,OutputOptions);
        Wavepackets.FormulaHatFinal = Wavepackets.PsiHat;
    end
        
    % clear up before saving
    Wavepackets.PsiHat = [];
    Wavepackets.Psi    = [];
        
    WPN = packWPN(Wavepackets,Potentials,Numerics);
    

end
