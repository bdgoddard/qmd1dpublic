function formulaHat=Formula2DSlicing(WPN,OutputOps)
% Applies the slicing algorithm using 1D formula to a 2D wavepacket.
[Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

direction = Numerics.formulaOptions.direction;
Numerics.direction = direction;
if(strcmp(direction,'down'))
    etaSign = -1;
else
    etaSign = +1;
end

% extract data from structs
NumPoints=Numerics.NumPoints;

ExpFn = str2func(Numerics.formulaOptions.ExpFunction); 
PreFn = str2func(Numerics.formulaOptions.PrefactorFunction); 

%LZFn = str2func(Numerics.formulaOptions.LZ);
Waitbar=OutputOps.Waitbar;

% Set up 2D grid
[~,~,xg,pg] = makeXP(Numerics);
% xg(1,:,:) is constant in rows
% xg(2,:,:) is constant in columns
Numerics.xg = xg;
Numerics.pg = pg;

% Set up 1D numerical grids for slices
Numerics1D.d = 1;
Numerics1D.xStart = Numerics.xStart;
Numerics1D.xEnd = Numerics.xEnd;
Numerics1D.xStep = Numerics.xStep;
Numerics1D.pStep = Numerics.pStep;
Numerics1D.epsilon = Numerics.epsilon;
Numerics1D.NumPoints = NumPoints(1);
[x1,p1,xg1D,pg1D] = makeXP(Numerics1D);
Numerics1D.xg = xg1D;
Numerics1D.pg = pg1D;


% Initialize the necessary vectors
formula = zeros(NumPoints);

% Get full 2D wavepacket at crossing point in momentum space
if(strcmp(direction,'down'))
    psiHatFn = Wavepackets.PsiHatUpCrossing;
else
    psiHatFn = Wavepackets.PsiHatDownCrossing;
end
psihat = makePsiHatFull(psiHatFn,Wavepackets,Numerics);

% And the corresponding wavepacket in position space to slice
psi = eIFTn(psihat,Numerics);

maxPsi = max(abs(psi(:)));
relCutoff = Numerics.formulaOptions.relativeCutoff * maxPsi;  

signP = sign(p1);
signP( p1 == 0 ) = 1; % fix case when p=0

if Waitbar==1
    wbe=waitbar(0,'Applying the slicing algorithm');
end
for n=1:NumPoints
    if Waitbar==1
        waitbar( n/NumPoints, wbe, 'Applying the slicing algorithm');
    end
    
    % constant in x2 slice
    psiupSlice = psi(:,n);
    
    % Cutoff to make computation quicker 
    if(max(abs(psiupSlice)>relCutoff))
   
        % Find Fourier transform of slice in 1D
        psiSliceHat = eFTn(psiupSlice,Numerics1D);

        % Find delta for spatial slice
        delta = min( makeRho(Potentials,x1,x1(n)) );  % uses that x2=x1

        % Find eta and the closest corresponding point in the momentum
        % vector
        eta = signP.*sqrt(p1.^2 + etaSign*4*delta);        
        [~,etaPos] = min( abs(bsxfun(@minus,eta.',p1)) );
        
        % Create momentum cutoff mask
        pMask = (p1.^2 + etaSign*4*delta)>0;
      
        % Determine wavepacket in eta
        etaPos = etaPos(pMask);
        psi0HatEta = zeros(size(p1));
        psi0HatEta(pMask) = psiSliceHat(etaPos);

        % Get exponential (LZ-like) factor
        ExpFactor = ExpFn(WPN,p1,0,0,x1(n));
        %LZ = LZFn(WPN,0,x1(n),p1(pMask),0);
        
        % Get prefactor
        Prefactor = PreFn(WPN,p1,x1(n));
        
        
        
        % Apply the formula to psi0HatEta
        %formulaSliceHat = zeros(size(p1));
        
        formulaSliceHat = Prefactor .* ExpFactor .* psi0HatEta;
        
        
%         formulaSliceHat(pMask) = ((p1(pMask)+eta(pMask))./(2*abs(eta(pMask)))) ...
%             .* LZ ...
%             .* psi0HatEta;

        % Add slice into transmitted spatial wavepacket
        formula(:,n) = eIFTn(formulaSliceHat,Numerics1D);
   
    end
    
end

% Determine 2D Fourier transform of combination of slices
formulaHat = eFTn(formula,Numerics);

if Waitbar==1
	close(wbe)
end