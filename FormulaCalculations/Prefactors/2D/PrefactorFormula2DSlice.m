function Prefactor = PrefactorFormula2DSlice(WPN,p,y)

    % Get eta and allowed p
    [eta,pCut,pMask] = makeEta2DSlice(WPN,p,y);

    Prefactor = zeros(size(p));
    Prefactor(pMask,1) = ((pCut+eta)./(2*abs(eta)));

end