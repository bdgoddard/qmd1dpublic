function Prefactor = PrefactorSign(WPN,p)

    Prefactor = sign(p);
    Prefactor(p==0) = 1;

end