function D4Rho = makeD4Rho1D(Potentials,x)

V1function = str2func(Potentials.V1);
V12function = str2func(Potentials.V12);

Parameters = Potentials.Parameters;

[V1,DV1,DDV1,D3V1,D4V1] = V1function(Parameters,x);
[V12,DV12,DDV12,D3V12,D4V12] = V12function(Parameters,x);

rho = (V1.^2 + V12.^2).^(1/2);

%[V1 DV1 DDV1 V12 DV12 DDV12]


D4Rho = 3 * (DDV1.^2 + DDV12.^2)./rho + 4 * (DV1.*D3V1 + DV12.*D3V12)./rho +(V1.*D4V1 + V12.*D4V12)/rho; 

end