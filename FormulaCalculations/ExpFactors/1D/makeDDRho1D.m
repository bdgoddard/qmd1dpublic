function DDRho = makeDDRho1D(Potentials,x)

V1function = str2func(Potentials.V1);
V12function = str2func(Potentials.V12);

Parameters = Potentials.Parameters;

[V1,DV1,DDV1] = V1function(Parameters,x);
[V2,DV2,DDV2] = V12function(Parameters,x);

rho = (V1.^2 + V2.^2).^(1/2);

% note that (V1.*DV1 + V2.*DV2)/delta = rho' = 0 at avoided crossing
DDRho = (V1.*DDV1 + DV1.^2 + V2.*DDV2 + DV2.^2)./rho - (V1.*DV1 + V2.*DV2).^2./(rho.^3);

end