function [ExpLZ,tau] = ExpQuadraticRho1D(WPN,p)    
%%% Constructs exponential prefactor using complex zero, and assuming   %%%
%%% Rho is quadratic in integral.                                       %%%

[~,Potentials,Numerics] = unpackWPN(WPN);

% find complex zero of Rho
AbsRho = @(x)abs(makeRho(Potentials,x(1)+1i*x(2)));
x=fminsearch(AbsRho,[0,0],optimset('TolX',1e-10));
cmplxZero=x(1)+1i*x(2);

% coefficients in quadratic
delta = makeRho(Potentials,0);
DDRho = makeDDRho1D(Potentials,0);
alpha=sqrt(delta.*DDRho);

% exact solution for integral using quadratic Rho
tau=cmplxZero.*sqrt(delta^2+alpha^2*cmplxZero^2) + ...
delta^2*log(2*sqrt(delta^2+alpha^2*cmplxZero^2)+2*alpha*cmplxZero)/alpha -...
delta^2*log(2*delta)/alpha;

% Construct Exp factor
epsilon = Numerics.epsilon;
[eta,pCut,pMask] = makeEta(WPN,p);
ExpLZ = zeros(size(p));
ExpLZ(pMask,1) = exp(-(imag(tau)/(2*delta*epsilon))*abs(pCut-eta))...
                   .*exp((-1i*real(tau))/(2*delta*epsilon)*(pCut-eta));
end

