function [ExpLZ,tau] = ExpRho1D(WPN,p)    

[~,Potentials,Numerics] = unpackWPN(WPN);

epsilon = Numerics.epsilon;

delta = makeRho(Potentials,0);

DDRho = makeDDRho1D(Potentials,0);

tau = pi*delta.^(3/2)/2./sqrt(DDRho);

ExpLZ = exp( - pi * delta^(3/2)/2/epsilon./abs(p)./sqrt(DDRho) );

end

