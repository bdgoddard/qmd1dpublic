function D3Rho = makeD3Rho1D(Potentials,x)

V1function = str2func(Potentials.V1);
V12function = str2func(Potentials.V12);

Parameters = Potentials.Parameters;

[V1,DV1,DDV1,D3V1] = V1function(Parameters,x);
[V12,DV12,DDV12,D3V12] = V12function(Parameters,x);

rho = (V1.^2 + V12.^2).^(1/2);

D3Rho = 3 * (DV1.*DDV1 + DV12.*DDV12)./rho + (V1.*D3V1 + V12.*D3V12)./rho; 

end