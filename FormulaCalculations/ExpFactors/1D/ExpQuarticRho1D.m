function [ExpLZ,tau] = ExpQuarticRho1D(WPN,p)    
%%% Constructs exponential prefactor using complex zero, and assuming   %%%
%%% Rho is quartic in integral (exact solution available but            %%%
%%% complicated!)                                                       %%%

[~,Potentials,Numerics] = unpackWPN(WPN);

% find complex zero of Rho
AbsRho = @(x)abs(makeRho(Potentials,x(1)+1i*x(2)));
x=fminsearch(AbsRho,[0,0],optimset('TolX',1e-10));
cmplxZero=x(1)+1i*x(2);

% coefficients in quadratic
delta = makeRho(Potentials,0);
DDRho = makeDDRho1D(Potentials,0);
D3Rho = makeD3Rho1D(Potentials,0);
D4Rho = makeD4Rho1D(Potentials,0);

alpha=sqrt(delta.*DDRho);

% Perform integral
dt=0.0001;
tVals=(0:dt:1).'; % transpose to make a column vector
curveVals=tVals.*cmplxZero; % the curve to integrate over

rhoVals=sqrt(delta^2+delta*DDRho*curveVals.^2+delta/3*D3Rho*curveVals.^3+(DDRho/4+D4Rho*delta^2/12)*curveVals.^4);

% 2 int_0^cmplxZero rho = 2 cmplxZero int_0^1 rho(t cmplxZero)
tau=2*cmplxZero*sum(rhoVals)*dt;

% Construct Exp factor
epsilon = Numerics.epsilon;
[eta,pCut,pMask] = makeEta(WPN,p);
ExpLZ = zeros(size(p));
ExpLZ(pMask,1) = exp(-(imag(tau)/(2*delta*epsilon))*abs(pCut-eta))...
                   .*exp((-1i*real(tau))/(2*delta*epsilon)*(pCut-eta));
end

