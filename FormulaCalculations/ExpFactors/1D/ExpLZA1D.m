function [ExpLZ,tau] = ExpLZA1D(WPN,p)    

[~,Potentials,Numerics] = unpackWPN(WPN);

epsilon = Numerics.epsilon;

V1Fn = str2func(Potentials.V1);
V12Fn = str2func(Potentials.V12);

Parameters = Potentials.Parameters;

[V1,DV1,DDV1] = V1Fn(Parameters,0);
[V12,DV12,DDV12] = V12Fn(Parameters,0);

delta = makeRho(Potentials,0);

DV = DV1.^2 + DV12.^2 ;

VD2V = V1.*DDV1 + V12.*DDV12;

tau = pi*delta.^2./2/sqrt( DV + VD2V );

denominator = sqrt( DV + VD2V ) .* abs(p);

ExpLZ = exp( - pi * delta^2/2/epsilon./denominator );


end

