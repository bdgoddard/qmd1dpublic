function [ExpLZ,tau] = ExpLZD1D(WPN,p)    

[Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

if(isfield(Wavepackets,'CoM'))
    CoM = Wavepackets.CoM;
else
    CoM = 0;
end

epsilon = Numerics.epsilon;

V1Fn = str2func(Potentials.V1);
V12Fn = str2func(Potentials.V12);

Parameters = Potentials.Parameters;

[~,DV1] = V1Fn(Parameters,CoM);
[~,DV12] = V12Fn(Parameters,CoM);

delta = makeRho(Potentials,CoM);

absDVp = abs(p).*sqrt( DV1.^2 + DV12.^2 );

tau = pi*delta.^2./2/sqrt( DV1.^2 + DV12.^2 );

ExpLZ = exp( - pi * delta^2/2/epsilon./absDVp );


end

