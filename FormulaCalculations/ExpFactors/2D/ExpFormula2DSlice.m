function [ExpFormula,tau] = ExpFormula2DSlice(WPN,p1,p2,x1,x2)

    [~,Potentials,Numerics] = unpackWPN(WPN);

    epsilon = Numerics.epsilon;

    delta = makeRho(Potentials,0,x2);
    tau   = makeTau2DSlice(WPN,x2);

    [eta,pCut,pMask] = makeEta2DSlice(WPN,p1,x2);

    ExpFormula = zeros(size(p1));
    
    ExpFormula(pMask,1) = exp(-(imag(tau)/(2*delta*epsilon))*abs(pCut-eta))...
                   .*exp((-1i*real(tau))/(2*delta*epsilon)*(pCut-eta));

end

