function ExpLZ = ExpLZD2D(WPN,p1,p2,x1,x2)    

[~,Potentials,Numerics] = unpackWPN(WPN);

epsilon = Numerics.epsilon;

DV = makeJacobian(Potentials,x1,x2);

delta = makeRho(Potentials,x1,x2);

DVp_1 = DV(1,1)*p1 + DV(1,2)*p2;
DVp_2 = DV(2,1)*p1 + DV(2,2)*p2;

DVp = [DVp_1 DVp_2];

absDVp = sqrt( sum( DVp.^2, 2 ) );

ExpLZ = exp( - pi * delta^2/2/epsilon./absDVp );

end

