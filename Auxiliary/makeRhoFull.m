function rho = makeRhoFull(Potentials,Numerics)

    [~,~,xg,~] = makeXP(Numerics);

    repGrid = replicateGrid(xg,Numerics);
    
    rho = makeRho(Potentials,repGrid{:});

end