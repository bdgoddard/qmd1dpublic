function repGrid = replicateGrid(grid,Numerics)

    NumPointRep = num2cell(ones(Numerics.d,1)*Numerics.NumPoints);
    repGrid = mat2cell(grid,NumPointRep{:},ones(Numerics.d,1));
end