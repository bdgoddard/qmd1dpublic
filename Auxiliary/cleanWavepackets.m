function Wavepackets = cleanWavepackets(Wavepackets)

%     clearList = {'PsiHat','Psi','EnergyOneLevel','MassOneLevel','MomentumOneLevel', ...
%     'PsiHatInitial','PsiHatUp','PsiHatDown','PsiUp','PsiDown', ...
%     'EnergyUp','EnergyDown','MassUp','MassDown','MomentumUp','MomentumDown'};
    
    clearList = {'PsiHat','Psi','PsiHatInitial','PsiHatUp','PsiHatDown','PsiUp','PsiDown', ...
                 'EnergyOneLevel','MassOneLevel','MomentumOneLevel'};

    nClear = length(clearList);
    for iClear = 1:nClear
        if(isfield(Wavepackets,clearList{iClear}))
            Wavepackets = rmfield(Wavepackets,clearList{iClear});
        end
    end

end
 
 