function [x1,x2]=quadratic_roots(a,b,c)
%%%              Roots of the quadratic polynomial equation             %%%
%%%                            ax^2+bx+c=0                              %%%

x1=(-b+sqrt(b.^2-4*a.*c))./(2.*a);
x2=(-b-sqrt(b.^2-4*a.*c))./(2.*a);   

%% Special Cases
% Not a quadratic
Check=(a==0);
x1(Check)=-c(Check)./b(Check);
x2(Check)=NaN;

% Equation is of form c=0 for some constant c;
Check=(a==0 & b==0);
x1(Check)=NaN;
x2(Check)=NaN;