function  [x1,x2,x3]=cubic_roots(a,b,c,d)
%%%          Provides all roots of the cubic polynomial:                %%%
%%%                     ax^3+bx^2+cx+d=0                                %%%

% Find Discriminants and other terms
Delta=18*a.*b.*c.*d-4.*b.^3.*d+b.^2.*c.^2-4*a.*c.^3-27.*a.^2.*d.^2;
Delta0=b.^2-3.*a.*c;
Delta1=2.*b.^3-9*a.*b.*c+27.*a.^2.*d;

% General Form
C=((Delta1+sqrt(Delta1.^2-4.*Delta0.^3))/2).^(1/3);
xi=-0.5+0.5*sqrt(3)*1i;

x1=(-1./(3.*a)).*(b+C+Delta0./C);
x2=(-1./(3.*a)).*(b+xi*C+Delta0./(xi*C));
x3=(-1./(3.*a)).*(b+xi.^2*C+Delta0./(xi.^2*C));

%% Special Cases - remove machine precision imaginary errors

% Delta=0 and Delta0=0 (one thrice-repeated root)
Check=(Delta0==0 & Delta==0);
x1(Check)=-b(Check)./(3.*a(Check));
x2(Check)=x1(Check); x3(Check)=x1(Check);

% Delta=0 and Delta0~=0  (all real roots, one double-repeated)
DoubleRoot=(9.*a.*d-b.*c)./(2.*Delta0);
SingleRoot=(4.*a.*b.*c-9.*a.^2.*d-b.^3)./(a.*Delta0);
Check=(Delta0~=0 & Delta==0);
x1(Check)=SingleRoot(Check);
x2(Check)=DoubleRoot(Check);
x3(Check)=x2(Check);

% Delta>0 (three distinct real roots)
Check=(Delta>0);
x1(Check)=real(x1(Check));
x2(Check)=real(x2(Check));
x3(Check)=real(x3(Check));

% Delta<0 (One real root, two complex roots)
p=(3*a.*c-b.^2)./(3*a.^2);
q=(2*b.^3-9*a.*b.*c+27*a.^2.*d)./(27*a.^3);

% if p<0 and 4p^3+27q^2>0
Check=(Delta<0 & p<0 & 4*p.^3+27*q.^2>0);
RealRoot=-2* (abs(q(Check))./q(Check)) .* sqrt(-p(Check)/3) .* cosh(1/3*acosh(-3*abs(q(Check)) ./ (2*p(Check)) .* sqrt(-3 ./ p(Check))))-b(Check)./(3*a(Check));
x1(Check)=RealRoot;
[x2(Check),x3(Check)]=quadratic_roots(a(Check),b(Check)+RealRoot.*a(Check),-d(Check)./RealRoot);

% if p>0
Check=(Delta<0 & p>0);
RealRoot=-2* sqrt(p/3) .* sinh(1/3* asinh((3* q ./ (2* p)).*sqrt(3./p)))-b./(3*a);
x1(Check)=RealRoot(Check);
[x2(Check),x3(Check)]=quadratic_roots(a(Check),b(Check)+RealRoot(Check).*a(Check),-d(Check)./RealRoot(Check));

% a=0 (i.e. quadratic)
Check=(a==0);
[x1(Check),x2(Check)]=quadratic_roots(b(Check),c(Check),d(Check));
x3(Check)=NaN;
end
    