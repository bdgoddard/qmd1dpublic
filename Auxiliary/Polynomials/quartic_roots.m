function [x1,x2,x3,x4]=quartic_roots(aIn,bIn,cIn,dIn,eIn)
%%%              Finds the roots of a quartic polynomial                %%%
%%%                       ax^4+bx^3+cx^2+dx+e=0                         %%%

% Methods taken from:
% Herbert E. Salzer "A Note on the Solution of Quartic Equations" (Am. Math Society Proceedings, 1959).

% Initialise the roots
x1=zeros(size(aIn)); x2=x1; x3=x1; x4=x1;
% divide by aIn
a=bIn./aIn; b=cIn./aIn; c=dIn./aIn; d=eIn./aIn;

% Define coefficients of a cubic equation
A=ones(size(a)); B=-b; C=a.*c-4*d; D=d.*(4*b-a.^2)-c.^2;
CubeRoot=cubic_1realroot(A,B,C,D);
mSq=0.25*a.^2-b+CubeRoot;
m=sqrt(mSq);
n=(a.*CubeRoot-2*c)./(4*m);
Check=(m==0);
n(Check)=sqrt(0.25*CubeRoot(Check).^2-d(Check));

% Real roots may be returned, should have zero imaginary part
Check=mSq>=0; 
alpha=0.5*a(Check).^2-CubeRoot(Check)-b(Check);
beta=4*n(Check)-a(Check).*m(Check);
gamma=sqrt(alpha+beta); delta=sqrt(alpha-beta);

x1(Check)=(-0.5*a(Check)+m(Check)+gamma)/2; 
x2(Check)=(-0.5*a(Check)+m(Check)-gamma)/2;
x3(Check)=(-0.5*a(Check)-m(Check)+delta)/2;
x4(Check)=(-0.5*a(Check)-m(Check)-delta)/2;

% Only imaginary roots are returned
Check=mSq<0; 
m(Check)=-1i*m(Check); n(Check)=-1i*n(Check);
alpha=0.5*a(Check).^2+CubeRoot(Check)-b(Check);
beta=4*n(Check)-a(Check).*m(Check);
rho=sqrt(alpha.^2+beta.^2);
gamma=sqrt(alpha+rho);
delta=beta./(2*gamma);

x1(Check)=(-0.5*a(Check)+gamma+1i*(m(Check)+delta))/2;
x2(Check)=conj(x1(Check));
x3(Check)=(-0.5*a(Check)-gamma+1i*(m(Check)-delta))/2;
x4(Check)=conj(x3(Check));

% Cases where aIn==0
Check=(aIn==0);
[x1(Check),x2(Check),x3(Check)]=cubic_roots(bIn(Check),cIn(Check),dIn(Check),eIn(Check));
x4(Check)=NaN;
end
