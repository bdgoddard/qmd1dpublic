function x1=cubic_1realroot(aIn,bIn,cIn,dIn)
%%%        Provides one real root of the cubic polynomial:              %%%
%%%                       ax^3+bx^2+cx+d=0                              %%%
a=bIn./aIn; b=cIn./aIn; c=dIn./aIn;

Q=(a.^2-3*b)/9; 
R=(2*a.^3-9*a.*b+27*c)/54; 
R2=R.^2; Q3=Q.^3;

A=-(abs(R)+sqrt(R2-Q3)).^(1/3); 
A(R < 0) = -A(R < 0) ;

B=Q./A;
B(A==0)=0;

x1=real(A+B-a/3); 