function psiHat = makePsiHatFull(psiHatString,Wavepackets,Numerics)

    if(strcmp(psiHatString(end-3:end),'.mat'))
        temp   = load(psiHatString);
        if(isstruct(temp.Data))
            psiHat = temp.Data.PsiHat;
        else
            psiHat = temp.Data;
        end
    else

        [~,~,~,pg] = makeXP(Numerics);
        psiHatFn = str2func(psiHatString);
        repGrid = replicateGrid(pg,Numerics);
    
        psiHat = psiHatFn(Wavepackets,repGrid{:});
    end
end