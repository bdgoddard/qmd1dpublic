function psi=eIFTn(psihat,Numerics)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the 2D inverse epsilon-Fourier transform of psi using Matlab's%
% inbuilt multidimensional Fourier transform routine and a scaling        %
% argument (see notes).                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xg=Numerics.xg;
dx=Numerics.xStep;
dp=Numerics.pStep;
pg=Numerics.pg;
NumPoints=Numerics.NumPoints;
d=Numerics.d;
epsilon=Numerics.epsilon;

% This only works for domains which are the same in each direction
First=num2cell(ones(1,d));
xFirst=xg(First{:},1);
X=sum((xg-xFirst)./dx,d+1);
P=sum(xFirst.*pg,d+1);

% shift before FT
epx1=exp((1i/epsilon)*P);
% shift after FT
epx2=exp(-pi*1i*X);
% renormalisation
N=(NumPoints^d)*(dp^d)/(2*pi*epsilon)^(d/2);
psi=N*epx2.*ifftn(epx1.*psihat);
end

