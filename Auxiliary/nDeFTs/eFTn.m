 function psihat=eFTn(psi,Numerics)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the nD epsilon-Fourier transform of psi using Matlab's        %
% inbuilt multidimensional Fourier transform routine and a scaling        %
% argument (see notes).   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xg=Numerics.xg;
dx=Numerics.xStep;
pg=Numerics.pg;
d=Numerics.d;
epsilon=Numerics.epsilon;

% This only works for domains which are the same in each direction
First=num2cell(ones(1,d));
xFirst=xg(First{:},1);
X=sum((xg-xFirst)./dx,d+1);
P=sum(xFirst*pg,d+1);

% shift of the function before applying FT
exp1=exp(pi*1i*X);
% shift after applying FT
exp2=exp((-1i/epsilon)*P);
% renormalisation (this is standard)
N=(dx^d)/(2*pi*epsilon)^(d/2); % must have different powers here
psihat=N*exp2.*fftn(exp1.*psi);

end

