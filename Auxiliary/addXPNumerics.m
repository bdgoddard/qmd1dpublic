function NumericsOut = addXPNumerics(Numerics)

    NumericsOut = Numerics;
    [x,p,xg,pg] = makeXP(Numerics);
    NumericsOut.x = x;
    NumericsOut.p = p;
    NumericsOut.xg = xg;
    NumericsOut.pg = pg;

end