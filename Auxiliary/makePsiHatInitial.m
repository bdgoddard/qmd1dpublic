function PsiHatInitial = makePsiHatInitial(WPN,OutputOptions)

    [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

    % if given initial wavepacket
    if(Wavepackets.givenInitial)

        if(isfield(Numerics,'direction'))
            direction = Numerics.direction;
        else
            direction = Numerics.formulaOptions.direction;
        end

        if(strcmp(direction,'down'))
            PsiHatInitial = Wavepackets.PsiHatUpInitial;
            PsiInitial    = Wavepackets.PsiUpInitial;
        else
            PsiHatInitial = Wavepackets.PsiHatDownInitial;
            PsiInitial    = Wavepackets.PsiDownInitial;
        end

        if(isempty(PsiHatInitial))

            if(isa(PsiInitial,'char'))
                PsiInitial = makePsiFull(PsiInitial,Wavepackets,Numerics);
            else
                PsiInitial = Wavepackets.PsiInitial;
            end

            NumericsTemp = addXPNumerics(Numerics);
            PsiHatInitial = eFTn(PsiInitial,NumericsTemp);

        else

            if(isa(PsiHatInitial,'char'))
                PsiHatInitial = makePsiHatFull(PsiHatInitial,Wavepackets,Numerics);
            end            

        end

        Wavepackets.PsiHatInitial = PsiHatInitial;
        
    else % if given wavepacket elsewhere
        
        Numerics.time   = Numerics.tBackward;
        Wavepackets.Psi = [];
        if(strcmp(Numerics.formulaOptions.direction,'down'))
            Wavepackets.PsiHat = Wavepackets.PsiHatUpCrossing;
            Potentials.V = 'makeVU';
        else
            Wavepackets.PsiHat = Wavepackets.PsiHatDownCrossing;
            Potentials.V = 'makeVL';
        end

        PsiHatCrossing = makePsiHatFull(Wavepackets.PsiHat,Wavepackets,Numerics);

        Wavepackets.PsiHat = PsiHatCrossing;
        
        WPN = packWPN(Wavepackets,Potentials,Numerics);
        
        % Evolve back to initial position and save
        WavepacketsI = ExactOneLevel(WPN,OutputOptions);
        PsiHatInitial = WavepacketsI.PsiHat;

        
    end
    
end