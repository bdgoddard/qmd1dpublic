function deleteData(OutputOptions)

    dirList = {'DataDirF','DataDirV','DataDirE','DataDirO'};

    for iDir = 1:length(dirList)
        display(OutputOptions.(dirList{iDir}))
    end

    prompt = 'Are you sure you want to delete the above\ndirectories and their contents? [y/n]';
    check = input(prompt,'s');

  
    if(strcmp(check,'y'))
        for iDir = 1:length(dirList)
            dataDir = OutputOptions.(dirList{iDir});
            if(exist(dataDir,'dir'))
                rmdir(dataDir,'s');
            end
        end

        disp('Data deleted');
    else
        disp('Aborted');
    end
    
end