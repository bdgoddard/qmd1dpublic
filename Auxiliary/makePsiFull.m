function psi = makePsiFull(psiString,Wavepackets,Numerics)

    if(strcmp(psiString(end-3:end),'.mat'))
        temp = load(psiString);
        if(isstruct(temp.Data))
            psi = temp.Data.Psi;
        else
            psi = temp.Data;
        end

    else

        [~,~,xg,~] = makeXP(Numerics);
        psiFn = str2func(psiString);
        repGrid = replicateGrid(xg,Numerics);

        psi = psiFn(Wavepackets,repGrid{:});
    end
        
end