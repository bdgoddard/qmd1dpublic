function PotentialsOut = PotentialsStruct(Numerics,PotentialsIn)

if(nargin<2)
    PotentialsIn = struct;
end

if isfield(Numerics,'d')
    d = Numerics.d;
else
    d = 2;
end

if isfield(PotentialsIn,'V1')
    PotentialsOut.V1 = PotentialsIn.V1;
else
    if (d==2)
        PotentialsOut.V1 = 'V1Default2D';
    else
        error(['No default potentials for dim = ' num2str(d)]);
    end
end

if isfield(PotentialsIn,'V2')
    PotentialsOut.V2 = PotentialsIn.V2;
else
    if (d==2)
        PotentialsOut.V2 = 'V2Default2D';
    else
        error(['No default potentials for dim = ' num2str(d)]);
    end
end

if isfield(PotentialsIn,'V12')
    PotentialsOut.V12 = PotentialsIn.V12;
else
    if (d==2)
        PotentialsOut.V12 = 'V12Default2D';
    else
        error(['No default potentials for dim = ' num2str(d)]);
    end
end

if isfield(PotentialsIn,'Parameters')
    PotentialsOut.Parameters = PotentialsIn.Parameters;
else
    PotentialsOut.Parameters = [];
end