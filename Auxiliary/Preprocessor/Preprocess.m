function [WPN,OutputOptions] = Preprocess(WPN,OutputOptions)

    [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

    Numerics = NumericsStruct(Numerics);
    OutputOptions = OutputOps(OutputOptions);
    Wavepackets = WavepacketsStruct(Numerics,Wavepackets);
    Potentials = PotentialsStruct(Numerics,Potentials);
    WPN = packWPN(Wavepackets,Potentials,Numerics);
    
end