function WavepacketsOut=WavepacketsStruct(Numerics,WavepacketsIn)


WavepacketsOut=struct;
WavepacketsOut.epsilon = Numerics.epsilon;

if nargin~=2
   	WavepacketsIn=struct;
%     disp('Default variables selected')
end


%%%%  Initial Conditions - start in either position or momentum space  %%%%

WavepacketsOut.givenInitial = isfield(WavepacketsIn,'PsiUpInitial') ...
               || isfield(WavepacketsIn,'PsiDownInitial') ...
               || isfield(WavepacketsIn,'PsiHatUpInitial') ...
               || isfield(WavepacketsIn,'PsiHatDownInitial');

% Initial Conditions - Position
if isfield(WavepacketsIn,'PsiUpInitial')
    WavepacketsOut.PsiUpInitial = WavepacketsIn.PsiUpInitial;
else
    WavepacketsOut.PsiUpInitial = [];
end
if isfield(WavepacketsIn,'PsiDownInitial')
    WavepacketsOut.PsiDownInitial = WavepacketsIn.PsiDownInitial;
else
    WavepacketsOut.PsiDownInitial = [];
end

% Initial Conditions - Momentum
if isfield(WavepacketsIn,'PsiHatUpInitial')
    WavepacketsOut.PsiHatUpInitial = WavepacketsIn.PsiHatUpInitial;
else
    WavepacketsOut.PsiHatUpInitial = [];

end
if isfield(WavepacketsIn,'PsiHatDownInitial')
    WavepacketsOut.PsiHatDownInitial = WavepacketsIn.PsiHatDownInitial;
else
    WavepacketsOut.PsiHatDownInitial = [];
end



%%%%  Crossing Conditions - start in either position or momentum space  %%%%
% Crossing Conditions - Position
if isfield(WavepacketsIn,'PsiUpCrossing')
    WavepacketsOut.PsiUpCrossing = WavepacketsIn.PsiUpCrossing;
else
    WavepacketsOut.PsiUpCrossing = [];
end
if isfield(WavepacketsIn,'PsiDownCrossing')
    WavepacketsOut.PsiDownCrossing = WavepacketsIn.PsiDownCrossing;
else
    WavepacketsOut.PsiDownCrossing = [];
end

% Crossing Conditions - Momentum
if isfield(WavepacketsIn,'PsiHatUpCrossing')
    WavepacketsOut.PsiHatUpCrossing = WavepacketsIn.PsiHatUpCrossing;
else
    if(~WavepacketsOut.givenInitial && ~isfield(WavepacketsIn,'PsiHatDownCrossing'))
        WavepacketsOut.PsiHatUpCrossing = 'psihatupDefault2D';
    else
        WavepacketsOut.PsiHatUpCrossing = [];
    end
end
if isfield(WavepacketsIn,'PsiHatDownCrossing')
    WavepacketsOut.PsiHatDownCrossing = WavepacketsIn.PsiHatDownCrossing;
else
    if(~WavepacketsOut.givenInitial && ~isfield(WavepacketsIn,'PsiHatUpCrossing'))
        WavepacketsOut.PsiHatDownCrossing = 'psihatdownDefault2D';
    else
        WavepacketsOut.PsiHatDownCrossing = [];
    end
end

if isfield(WavepacketsIn,'Parameters')
    WavepacketsOut.Parameters = WavepacketsIn.Parameters;
    if(~isfield(WavepacketsOut.Parameters,'epsilon'))
        WavepacketsOut.Parameters.epsilon = Numerics.epsilon;
    end
else
    WavepacketsOut.Parameters = [];
end


% Other structs that could be added to WavePacketsOut are Fourier
% transforms of initial conditions, initial wavepackets in diabatic
% representation, wavepacket after dynamics, wavepacket
% approximated by formula, energy calculation, momentum, centre of mass,
% lower mass. All need to be calculated.

end