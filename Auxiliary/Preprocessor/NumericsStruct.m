 function NumericsOut=NumericsStruct(NumericsIn)

NumericsOut=struct;
if nargin==0
   	NumericsIn=struct;
%     disp('Default variables selected')
end

% Small parameter epsilon
if isfield(NumericsIn,'epsilon')
    NumericsOut.epsilon=NumericsIn.epsilon;
else
    %NumericsOut.epsilon=1/30;
    NumericsOut.epsilon=1/10;
end

%%%%%%%%%%%%%%%%%%                Grid                     %%%%%%%%%%%%%%%%

% Set the dimension
if isfield(NumericsIn,'d')
    NumericsOut.d=NumericsIn.d;
else
    NumericsOut.d=2;
end

% Set the number of points in each dimension
if isfield(NumericsIn,'NumPoints')
    NumericsOut.NumPoints=NumericsIn.NumPoints;
else
    %NumericsOut.NumPoints=2^11;
    NumericsOut.NumPoints=2^10;
end

% Set space grid options - momentum one is derived from this
if isfield(NumericsIn,'xStart')
    NumericsOut.xStart=NumericsIn.xStart;
else
    NumericsOut.xStart=-15;
end
if isfield(NumericsIn,'xEnd')
    NumericsOut.xEnd=NumericsIn.xEnd;
else
    NumericsOut.xEnd=15;
end
if isfield(NumericsIn,'xStep')
    NumericsOut.xStep=NumericsIn.xStep;
else
    NumericsOut.xStep=(NumericsOut.xEnd-NumericsOut.xStart)./(NumericsOut.NumPoints-1);
end

NumericsOut.pStep = 2.*pi.*NumericsOut.epsilon./(NumericsOut.NumPoints.*NumericsOut.xStep);

%
%%%%%%%%%%%%%%%%%%                Time                     %%%%%%%%%%%%%%%%
% Length of simulation
if isfield(NumericsIn,'tBackward')
    NumericsOut.tBackward=NumericsIn.tBackward;
else
    NumericsOut.tBackward=-2;
end

if isfield(NumericsIn,'tForward')
    NumericsOut.tForward=NumericsIn.tForward;
else
    NumericsOut.tForward=2;
end

NumericsOut.tTotal = NumericsOut.tForward - NumericsOut.tBackward;


% Timestep
if isfield(NumericsIn,'timestep')
    NumericsOut.timestep=NumericsIn.timestep;
else
    NumericsOut.timestep=0.01;
end

% Compare at crossing (true) or at final time (false)
if(isfield(NumericsIn,'compareAtCrossing'))
    NumericsOut.compareAtCrossing = NumericsIn.compareAtCrossing;
else
    NumericsOut.compareAtCrossing = true;
end

%%%%%%%%%%%%%%%%%%             Formula                     %%%%%%%%%%%%%%%%

% Formula
if isfield(NumericsIn,'formula')
    NumericsOut.formula = NumericsIn.formula;
else
    if(NumericsOut.d==1)
        NumericsOut.formula = 'Formula1D';
    elseif(NumericsOut.d==2)
        %NumericsOut.formula = 'FormulaNoTrace2DSlicing';
        NumericsOut.formula = 'Formula2DSlicing';
    else
        error(['No default formula for dim = ' num2str(NumericsOut.d)]);
    end
end

% Remainder Formula
if isfield(NumericsIn,'RemainderFormula')
    NumericsOut.RemainderFormula = NumericsIn.RemainderFormula;
else
    if(NumericsOut.d==1)
        NumericsOut.RemainderFormula = 'RemainderFormula1D';
    elseif(NumericsOut.d==2)
        %NumericsOut.formula = 'FormulaNoTrace2DSlicing';
        disp(['No default remainder formula for dim = ' num2str(NumericsOut.d)]);
%         NumericsOut.formula = 'Formula2DSlicing';
    else
        error(['No default remainder formula for dim = ' num2str(NumericsOut.d)]);
    end
end

% Formula options
if(isfield(NumericsIn,'formulaOptions'))
    NumericsOut.formulaOptions = NumericsIn.formulaOptions;
else
    NumericsOut.formulaOptions = struct;
end

% Exponential part
if ~isfield(NumericsOut.formulaOptions,'ExpFunction')
    if(NumericsOut.d==1)
        NumericsOut.formulaOptions.ExpFunction = 'ExpFormula1D';
    elseif(NumericsOut.d==2)
        NumericsOut.formulaOptions.ExpFunction = 'ExpFormula2DSlice';
    else
        error(['No default exponential function for dim = ' num2str(NumericsOut.d)]);
    end
end

% Prefactor part
if ~isfield(NumericsOut.formulaOptions,'PrefactorFunction')
    if(NumericsOut.d==1)
        NumericsOut.formulaOptions.PrefactorFunction = 'PrefactorFormula1D';
    elseif(NumericsOut.d==2)
        NumericsOut.formulaOptions.PrefactorFunction = 'PrefactorFormula2DSlice';
    else
        error(['No default prefactor function for dim = ' num2str(NumericsOut.d)]);
    end
end

% % Tau
% if ~isfield(NumericsOut.formulaOptions,'tau')
%     if(NumericsOut.d==1)
%         NumericsOut.formulaOptions.tau = 'makeTau1D';
%     elseif(NumericsOut.d==2)
%         NumericsOut.formulaOptions.tau = 'makeTau2DSlice';
%     else
%         error(['No default tau for dim = ' num2str(NumericsOut.d)]);
%     end
% end

% Cutoff
if(~isfield(NumericsOut.formulaOptions,'relativeCutoff'))
    NumericsOut.formulaOptions.relativeCutoff = 10^-6;
end

% Transition direction
if(~isfield(NumericsOut.formulaOptions,'direction'))
    NumericsOut.formulaOptions.direction = 'down';
end
