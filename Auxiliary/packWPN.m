function WPN = packWPN(Wavepackets,Potentials,Numerics)
    
    if(nargin<1 || isempty(Wavepackets))
        Wavepackets = struct;
    end
    if(nargin<2 || isempty(Potentials))
        Potentials = struct;
    end
    if(nargin<3 || isempty(Numerics))
        Numerics = struct;
    end

    WPN.Wavepackets = Wavepackets;
    WPN.Potentials  = Potentials;
    WPN.Numerics    = Numerics;
end