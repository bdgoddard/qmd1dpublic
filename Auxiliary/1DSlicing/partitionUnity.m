function [centres,width] = partitionUnity(a,b,n)

    dx = (b-a)/n;
    width = 2*dx;
    
    xMin = a;

    centres = xMin + (0:n)*dx;
    
end