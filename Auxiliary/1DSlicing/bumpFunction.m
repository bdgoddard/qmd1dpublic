function bump = bumpFunction(x,centre,width)
% x0 = first non-zero value
% onesWidth = width of constant ones = width of two smooth curves
    
    bump = zeroOne(x + width/2 - centre,width/2) + zeroOne(width/2 - x + centre,width/2) - 1;

end

function f = piecewise(x)
    f = zeros(size(x));
    mask = (x>0);
    f(mask) = exp(-1./x(mask));
end

function g = zeroOne(x,a)
    g = piecewise(x/a) ./ ( piecewise(x/a) + piecewise((a-x)/a) );
end
