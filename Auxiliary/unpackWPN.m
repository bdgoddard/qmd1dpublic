function [Wavepackets,Potentials,Numerics] = unpackWPN(WPN)

    if(~isstruct(WPN))
        WPN = struct;
    end

    if(isfield(WPN,'Wavepackets'))
        Wavepackets = WPN.Wavepackets;
    else
        Wavepackets = struct;
    end
    
    if(isfield(WPN,'Potentials'))
        Potentials  = WPN.Potentials;
    else
        Potentials = struct;
    end
    
    if(isfield(WPN,'Numerics'))
        Numerics    = WPN.Numerics;
    else
        Numerics = struct;
    end
end