function fixContourPlot(ha,hc,Options)

    axis(ha,'equal');
        
    doColorbar = true;
    
    if(isfield(Options,'colorbar') && ~Options.colorbar)
        doColorbar = false;
    end
    
    hcb = colorbar(ha);
    
    if(~doColorbar)
        set(hcb,'Visible','off');
    end
    
    if(isfield(Options,'LineWidth'))
        set(hc,'LineWidth',Options.LineWidth);
    end
    
    if(isfield(Options,'Colour'))
        set(hc,'LineColor',Options.Colour);
    end
    
    if(isfield(Options,'Style'))
        set(hc,'LineStyle',Options.Style);
    end
    
    if(isfield(Options,'x1'))
        xlabel(ha,Options.x1,'Interpreter','Latex');
    end
    
    if(isfield(Options,'x2'))
        ylabel(ha,Options.x2,'Interpreter','Latex');
    end

    if(isfield(Options,'title'))
        title(ha,Options.title,'Interpreter','Latex');
    end

    
end