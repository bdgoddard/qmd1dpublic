function PlotPsiHat1D(PsiHat,Numerics,Options)

    if(isfield(Options,'axes'))
        ha = Options.axes;
    else
        ha = axes;
    end
    
    [x,p,~,~] = makeXP(Numerics);

    pPlot = p;
    PsiHatPlot = PsiHat;

    makeMask = false;

    if( isfield(Options,'pMin') || isfield(Options,'pMax') )
        makeMask = true;
    end

    
    if(makeMask)
        if(isfield(Options,'pMin'))
            pMin = Options.pMin;
        else
            pMin = p(1);
        end

        if(isfield(Options,'pMax'))
            pMax = Options.pMax;
        else
            pMax = p(end);
        end
    
        pMask = ( p >= pMin ) & (p <= pMax);
        
    else
        if(isfield(Options,'pMask'))
            pMask = Options.pMask;
        else
            pMask = true(size(x));
        end

    end

    pPlot(~pMask,:) = [];
    PsiHatPlot(~pMask,:) = [];

    if(isfield(Options,'phase') && Options.phase)
%         hl = plot(ha,pPlot,mod(angle(PsiHatPlot),2*pi));
        hl = plot(ha,pPlot,angle(PsiHatPlot));
    else
        hl = plot(ha,pPlot,abs(PsiHatPlot));
    end
    
    Options.xLim = [pPlot(1),pPlot(end)];
    
    Options.x = '$p$';
    fixPlot(ha,hl,Options)
    
end