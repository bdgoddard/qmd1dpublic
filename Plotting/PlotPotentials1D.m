function PlotPotentials1D(Potentials,Numerics)

    [x,~,~,~] = makeXP(Numerics);
    
    Vup = makeVU(Potentials,x);
    Vdown = makeVL(Potentials,x);
    
    figure
    plot(x,Vup)
    hold on
    plot(x,Vdown)
    
end