function PlotPsiHat2D(PsiHat,Numerics,Options)

    if(isfield(Options,'axes'))
        ha = Options.axes;
    else
        ha = axis;
    end

    [~,p,~,pg] = makeXP(Numerics);

    pg1 = pg(:,:,1);
    pg2 = pg(:,:,2);
    p1 = p(:,1);
    p2 = p(:,2);

    pg1Plot = pg1;
    pg2Plot = pg2;
    PsiHatPlot = PsiHat;

    
%     surf(pg(:,:,1),pg(:,:,2),abs(PsiHat));
    
    makeMask = false;

    if( isfield(Options,'pMin') || isfield(Options,'pMax') )
        makeMask = true;
    end

    
    if(makeMask)
        if(isfield(Options,'pMin'))
            p1Min = Options.pMin(1);
            p2Min = Options.pMin(2);
        else
            p1Min = p(1);
            p2Min = p(1);
        end

        if(isfield(Options,'pMin'))
            p1Max = Options.pMax(1);
            p2Max = Options.pMax(2);
        else
            p1Max = p(end);
            p2Max = p(end);
        end
    
        p1Mask = ( p1 >= p1Min ) & (p1 <= p1Max);
        p2Mask = ( p2 >= p2Min ) & (p2 <= p2Max);
        
    else
        if(isfield(Options,'pMask'))
            pMask = Options.pMask;
        else
            pMask = true(size(p));
        end
        
        p1Mask = pMask(:,1);
        p2Mask = pMask(:,1);

    end


    % remove p1 out of bound
    pg1Plot(~p1Mask,:) = [];
    pg2Plot(~p1Mask,:) = [];
    PsiHatPlot(~p1Mask,:) = [];
        
    % remove p2 out of bound
    pg1Plot(:,~p2Mask) = [];
    pg2Plot(:,~p2Mask) = [];
    PsiHatPlot(:,~p2Mask) = [];

    if(isfield(Options,'contours'))
        contours = Options.contours;
        [~,hc] = contour(ha,pg1Plot,pg2Plot,abs(PsiHatPlot).^2,contours);
    else
        [~,hc] = contour(ha,pg1Plot,pg2Plot,abs(PsiHatPlot).^2);
    end

    Options.x1 = '$p_1$';
    Options.x2 = '$p_2$';
    
    fixContourPlot(ha,hc,Options)
        
end