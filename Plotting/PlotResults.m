function PlotResults(WPN)

[W,P,N] = unpackWPN(WPN);
N = addXPNumerics(N);

direction = N.formulaOptions.direction;

if(strcmp(direction,'down'))
    ExactHatCrossing = W.ExactHatDownCrossing;
else
    ExactHatCrossing = W.ExactHatUpCrossing;
end


FormulaHatCrossing = W.FormulaHatCrossing;

ExactCrossing = eIFTn(ExactHatCrossing,N);
%FormulaCrossing = eIFTn(FormulaHatCrossing,N);

ExactMeanPos = sum(N.x .* abs(ExactCrossing).^2 * N.xStep)

ExactMass = sum(abs(ExactHatCrossing).^2 * N.pStep)
FormulaMass = sum(abs(FormulaHatCrossing).^2 * N.pStep);
MassError = (ExactMass-FormulaMass)

figure

plot(N.p,abs(ExactHatCrossing),'b');
hold on
plot(N.p,abs(FormulaHatCrossing),'--r');


figure

plot(N.p,angle(ExactHatCrossing),'b');
hold on
plot(N.p,angle(FormulaHatCrossing),'--r');
plot(N.p,angle(-FormulaHatCrossing),'--g');

end