function PlotExactFormulaEnd1D(WPN,Options)

    if(nargin<2)
        Options = struct;
    end

    [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

    direction = Numerics.formulaOptions.direction;
    down = strcmp(direction,'down');

    figure('Position',[50,50,1200,800]);
    
    PsiHatF = Wavepackets.PsiHatFormula;
        
    if(down)
        PsiHatE = Wavepackets.PsiHatDown;
        PsiHat0 = Wavepackets.PsiHatUp;
    else
        PsiHatE = Wavepackets.PsiHatUp;
        PsiHat0 = Wavepackets.PsiHatDown;
    end
    
    Numerics = addXPNumerics(Numerics);
    
    if(isfield(Numerics.formulaOptions,'relativeCutoff'))
        Numerics.cutoff = sqrt(Numerics.formulaOptions.relativeCutoff);
    else
        Numerics.cutoff = 10^-5;
    end
    
    PsiF = eIFTn(PsiHatF,Numerics);
    Psi0 = eIFTn(PsiHat0,Numerics);
    
    % get bounds
    [pMinF,pMaxF] = makePBounds(PsiHatF,Numerics);
    [pMin0,pMax0] = makePBounds(PsiHat0,Numerics);

    pMin = min(pMinF,pMin0);
    pMax = max(pMaxF,pMax0);
    
    Options.pMin = pMin;
    Options.pMax = pMax;

    [xMinF,xMaxF] = makeXBounds(PsiF,Numerics);
    [xMin0,xMax0] = makeXBounds(Psi0,Numerics);

    xMin = min(xMinF,xMin0);
    xMax = max(xMaxF,xMax0);

    Options.xMin = xMin;
    Options.xMax = xMax;
    
    ha = tightsubplot(2,2,[0.2,0.1],0.15,0.1);
    
    if(down)
        PsiHatU = PsiHat0;
        PsiHatD = PsiHatF;
    else
        PsiHatU = PsiHatF;
        PsiHatD = PsiHat0;
    end
        
    Options.axes = ha(1);
    Options.title = 'Upper';
    Options.Colour = 'r';
    Options.Style = '-';
    
    PlotPsiHat1D(PsiHatU,Numerics,Options);

    Options.axes = ha(3);
    Options.title = 'Lower';
    Options.Colour = 'b';
    Options.Style = '-';

    PlotPsiHat1D(PsiHatD,Numerics,Options);    

    Options.axes = ha(4);    
    Options.title = 'Error';
    Options.Colour = 'k';
    Options.Style = '-';

    mE = sum(abs(PsiHatF - PsiHatE));
    pE = sum(abs(PsiHatF + PsiHatE));
    if(mE < pE)
        sgn = +1;
    else
        sgn = -1;
    end
    PsiHatE = sgn * PsiHatE;
        
    PlotPsiHat1D(PsiHatF - PsiHatE,Numerics,Options);

    OptionsPhase = Options;
    OptionsPhase.phase = true;
    OptionsPhase.axes = ha(2);
    OptionsPhase.title = 'Phase Error';
    
    OptionsPhase.Colour = 'r';
    OptionsPhase.Style = '-';
    
    %PlotPsiHat1D(PsiHatE,Numerics,OptionsPhase);
    
    %hold(ha(2),'on');

    OptionsPhase.Colour = 'b';
    OptionsPhase.Style = '--';

    %PlotPsiHat1D(PsiHatF,Numerics,OptionsPhase);

    OptionsPhase.Colour = 'k';
    OptionsPhase.Style = '-';
    PsiAngleError = angle(PsiHatF) - angle(PsiHatE);
    PsiAngleErrorExp = exp(1i * PsiAngleError);
    PlotPsiHat1D(PsiAngleErrorExp,Numerics,OptionsPhase);
    ha(2);
    set(ha(2),'yLim',[-0.1,0.1]);
    
%     yLim = get(ha(4),'yLim');
%     yMax = yLim(2);
%     yLim = 30 * [-yMax,yMax];
    %set(ha(2),'yLim',yLim);

end
