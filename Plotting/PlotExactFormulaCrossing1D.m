function PlotExactFormulaCrossing1D(WPN,Options)

    if(nargin<2)
        Options = struct;
    end

    [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

    figure('Position',[50,50,1200,800]);
    
    PsiHatF = Wavepackets.FormulaHatCrossing;
    
    direction = Numerics.formulaOptions.direction;
    down = strcmp(direction,'down');
    
    if(down)
        PsiHatE = Wavepackets.ExactHatDownCrossing;
        psiHatFn = Wavepackets.PsiHatUpCrossing;
    else
        PsiHatE = Wavepackets.ExactHatUpCrossing;
        psiHatFn = Wavepackets.PsiHatDownCrossing;
    end
        
    PsiHatC = makePsiHatFull(psiHatFn,Wavepackets,Numerics);
    
    Numerics = addXPNumerics(Numerics);
    
    if(isfield(Numerics.formulaOptions,'relativeCutoff'))
        Numerics.cutoff = sqrt(Numerics.formulaOptions.relativeCutoff);
    else
        Numerics.cutoff = 10^-5;
    end
    
    PsiF = eIFTn(PsiHatF,Numerics);
    PsiC = eIFTn(PsiHatC,Numerics);
    
    % get bounds
    [pMinF,pMaxF] = makePBounds(PsiHatF,Numerics);
    [pMinC,pMaxC] = makePBounds(PsiHatC,Numerics);

    pMin = min(pMinF,pMinC);
    pMax = max(pMaxF,pMaxC);
    
    Options.pMin = pMin;
    Options.pMax = pMax;

    [xMinF,xMaxF] = makeXBounds(PsiF,Numerics);
    [xMinC,xMaxC] = makeXBounds(PsiC,Numerics);

    xMin = min(xMinF,xMinC);
    xMax = max(xMaxF,xMaxC);

    Options.xMin = xMin;
    Options.xMax = xMax;
    
    ha = tightsubplot(2,2,[0.2,0.1],0.15,0.1);
    
    if(down)
        PsiHatU = PsiHatC;
        PsiHatD = PsiHatF;
    else
        PsiHatU = PsiHatF;
        PsiHatD = PsiHatC;
    end
        
    Options.axes = ha(1);
    Options.title = 'Upper';
    Options.Colour = 'r';
    Options.Style = '-';
    
    PlotPsiHat1D(PsiHatU,Numerics,Options);

    Options.axes = ha(3);
    Options.title = 'Lower';
    Options.Colour = 'b';
    Options.Style = '-';

    PlotPsiHat1D(PsiHatD,Numerics,Options);    
    
    OptionsPhase = Options;
    OptionsPhase.phase = true;
    OptionsPhase.axes = ha(2);
    OptionsPhase.title = 'Phase';
    
    OptionsPhase.Colour = 'r';
    OptionsPhase.Style = '-';
    
    PlotPsiHat1D(PsiHatU,Numerics,OptionsPhase);
    
    hold(ha(2),'on');

    OptionsPhase.Colour = 'b';
    OptionsPhase.Style = '-';

    PlotPsiHat1D(PsiHatD,Numerics,OptionsPhase);

    OptionsPhase.Colour = 'k';
    OptionsPhase.Style = '--';
    OptionsPhase.phase = false;
    PsiAngleError = abs(angle(PsiHatF) - angle(PsiHatE));
    PlotPsiHat1D(PsiAngleError,Numerics,OptionsPhase);
    
    
%     OptionsSpace = Options;
%     OptionsSpace.colorbar = false;
%     OptionsSpace.axes = ha(2);
%     OptionsSpace.title = 'Physical Space';
% 
%     OptionsSpace.Colour = 'r';
%     OptionsSpace.Style = '-';
% 
%     if(down)
%         PlotPsi1D(PsiC,Numerics,OptionsSpace);
%     else
%         PlotPsi1D(PsiF,Numerics,OptionsSpace);
%     end
% 
%     hold(ha(2), 'on');
% 
%     OptionsSpace.Colour = 'b';
%     OptionsSpace.Style = '-';
% 
%     if(down)
%         PlotPsi1D(PsiF,Numerics,OptionsSpace);
%     else
%         PlotPsi1D(PsiC,Numerics,OptionsSpace);
%     end

        
    Options.axes = ha(4);    
    Options.title = 'Error';
    Options.Colour = 'k';
    Options.Style = '-';

    
    mE = sum(abs(PsiHatF - PsiHatE));
    pE = sum(abs(PsiHatF + PsiHatE));
    if(mE < pE)
        sgn = -1;
    else
        sgn = +1;
    end
    
    PlotPsiHat1D(PsiHatF + sgn * PsiHatE,Numerics,Options);

end
