function PlotPsi1D(Psi,Numerics,Options)

    if(isfield(Options,'axes'))
        ha = Options.axes;
    else
        ha = axes;
    end
    
    [x,~,~,~] = makeXP(Numerics);

    xPlot = x;
    PsiPlot = Psi;

    makeMask = false;

    if( isfield(Options,'xMin') || isfield(Options,'xMax') )
        makeMask = true;
    end

    
    if(makeMask)
        if(isfield(Options,'xMin'))
            xMin = Options.xMin;
        else
            xMin = x(1);
        end

        if(isfield(Options,'xMax'))
            xMax = Options.xMax;
        else
            xMax = x(end);
        end
    
        xMask = ( x >= xMin ) & (x <= xMax);
        
    else
        if(isfield(Options,'xMask'))
            xMask = Options.xMask;
        else
            xMask = true(size(x));
        end

    end

    xPlot(~xMask,:) = [];
    PsiPlot(~xMask,:) = [];

    hl = plot(ha,xPlot,abs(PsiPlot));

    Options.xLim = [xPlot(1),xPlot(end)];
    
    Options.x = '$x$';
    fixPlot(ha,hl,Options)
    
end