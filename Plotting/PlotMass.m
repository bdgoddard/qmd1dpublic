function PlotMass(WPN,OutputOptions)

    [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);
    
    direction = Numerics.formulaOptions.direction;
    down = strcmp(direction,'down');
    
    formula = isfield(Wavepackets,'MassFormula');
    
    nTimes = length(Wavepackets.MassUp);
    times = (1:nTimes) * Numerics.timestep;
    
    hf = figure('Position',[50,50,1200,800]);
    ha = axes;
    
    if(down)
        mAdiabatic = Wavepackets.MassDown;
    else
        mAdiabatic = Wavepackets.MassUp;
    end
    plot(times,mAdiabatic,'k');
    
    if(formula)
        hold on
        plot(times,Wavepackets.MassFormula,'r');
    end
    
    yMax = 2*mAdiabatic(end);
    ylim([0,yMax]);
    
    hs = axes('Position',[.2 .65 .2 .2]);
    box on
    plot(times,mAdiabatic,'k')
    xlim([0,max(times)]);
    
    if(~formula)
        hold(ha,'on');
        tZero = -Numerics.tBackward;
        plot(ha,[tZero,tZero],[0,yMax],'--k');
    end
    
    xlabel(ha,'Time');
    ylabel(ha,'Mass');
    
    pdfFile = [OutputOptions.DataDirO filesep 'MassEvolution-' getTimeStr() '.pdf'];
    save2pdf(pdfFile,hf);
    
    
end