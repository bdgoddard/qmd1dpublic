function PlotDiabaticEigenvectors1D(Potentials,Numerics)

    [x,~,~,~] = makeXP(Numerics);

    [~,X,Z,~] = makeRhoXZTrace(Potentials,x);
    
    % deal with the case when X is close to zero
    mask = abs(X)<10^(-15);
    maskPos = mask & (Z>=0);
    maskNeg = mask & (Z<0);

    % upper level
    Phi1plus=(Z+sqrt(Z.^2+X.^2))./X;
    % (Z + abs(Z))/X + O(X)
    Phi1minus=ones(size(Phi1plus));

    % deal with region where X is small
    Phi1plus(maskPos) = 1;
    Phi1minus(maskPos) = 0;
    Phi1plus(maskNeg) = 0;
    Phi1minus(maskNeg) = 1;

    % normalization
    k1=1./sqrt(Phi1plus.^2+Phi1minus.^2);
    Phi1plus=k1.*Phi1plus;
    Phi1minus=k1.*Phi1minus;

    % lower level
    Phi2plus=(Z-sqrt(Z.^2+X.^2))./X;
    % (Z - abs(Z))/X + O(X)
    Phi2minus=ones(size(Phi2plus));

    % deal with region where X is small
    Phi2plus(maskPos) = 0;
    Phi2minus(maskPos) = 1;
    Phi2plus(maskNeg) = -1;
    Phi2minus(maskNeg) = 0;

    % normalization
    k2=1./sqrt(Phi2plus.^2+Phi2minus.^2);
    Phi2plus=k2.*Phi2plus;
    Phi2minus=k2.*Phi2minus;

    
    figure('Position',[50,50,1200,800]);
    plot(x,Phi1plus,'r')
    hold on
    plot(x,Phi1minus,'--r')
    plot(x,Phi2plus,'b')
    plot(x,Phi2minus,'--b')

    ylabel('Potential');
    xlabel('$x$','Interpreter','latex');
    
end