function [pMin,pMax] = makePBounds(PsiHat,Numerics)

    [~,p,~,~] = makeXP(Numerics);
    
    if(isfield(Numerics,'cutoff'))
        cutoff = Numerics.cutoff;
    else
        cutoff = 10^-2;
    end

    PsiHat = abs(PsiHat).^2;
    
    maxPsiHat = max(PsiHat(:));
    bound = maxPsiHat*cutoff;
    PsiHatMask = ( PsiHat > bound );

    d = Numerics.d;
    
    if(d==2)
    
        p1Mask = sum(PsiHatMask,2);
        p2Mask = sum(PsiHatMask,1);

        p1Mask = p1Mask(:);
        p2Mask = p2Mask(:);

        p1MinPos = find(p1Mask,1,'first');
        p1MaxPos = find(p1Mask,1,'last');
        p2MinPos = find(p2Mask,1,'first');
        p2MaxPos = find(p2Mask,1,'last');

        p1Min = p(p1MinPos);
        p1Max = p(p1MaxPos);
        p2Min = p(p2MinPos);
        p2Max = p(p2MaxPos);

        pMin = [p1Min; p2Min];
        pMax = [p1Max; p2Max];

    elseif(d==1)
        pMask = PsiHatMask;
        pMinPos = find(pMask,1,'first');
        pMaxPos = find(pMask,1,'last');
        pMin = p(pMinPos);
        pMax = p(pMaxPos);
        
    end
    
end