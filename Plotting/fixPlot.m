function fixPlot(ha,hl,Options)
    
    if(isfield(Options,'xLim'))
        set(ha,'xLim',Options.xLim);
    end

    if(isfield(Options,'LineWidth'))
        set(hl,'LineWidth',Options.LineWidth);
    end
    
    if(isfield(Options,'Colour'))
        set(hl,'Color',Options.Colour);
    end
    
    if(isfield(Options,'Style'))
        set(hl,'LineStyle',Options.Style);
    end
    
    if(isfield(Options,'x'))
        xlabel(ha,Options.x,'Interpreter','Latex');
    end
    
    if(isfield(Options,'y'))
        ylabel(ha,Options.y,'Interpreter','Latex');
    end

    if(isfield(Options,'title'))
        title(ha,Options.title,'Interpreter','Latex');
    end

    
end