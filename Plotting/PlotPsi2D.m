function PlotPsi2D(Psi,Numerics,Options)

    if(isfield(Options,'axes'))
        ha = Options.axes;
    else
        ha = axis;
    end
    
    [x,~,xg,~] = makeXP(Numerics);

    xg1 = xg(:,:,1);
    xg2 = xg(:,:,2);
    x1 = x(:,1);
    x2 = x(:,2);

    xg1Plot = xg1;
    xg2Plot = xg2;
    PsiPlot = Psi;

    
    makeMask = false;

    if( isfield(Options,'xMin') || isfield(Options,'xMax') )
        makeMask = true;
    end

    
    if(makeMask)
        if(isfield(Options,'xMin'))
            x1Min = Options.xMin(1);
            x2Min = Options.xMin(2);
        else
            x1Min = x(1);
            x2Min = x(1);
        end

        if(isfield(Options,'xMin'))
            x1Max = Options.xMax(1);
            x2Max = Options.xMax(2);
        else
            x1Max = x(end);
            x2Max = x(end);
        end
    
        x1Mask = ( x1 >= x1Min ) & (x1 <= x1Max);
        x2Mask = ( x2 >= x2Min ) & (x2 <= x2Max);
        
    else
        if(isfield(Options,'xMask'))
            xMask = Options.xMask;
        else
            xMask = true(size(x));
        end
        
        x1Mask = xMask(:,1);
        x2Mask = xMask(:,1);

    end


    % remove p1 out of bound
    xg1Plot(~x1Mask,:) = [];
    xg2Plot(~x1Mask,:) = [];
    PsiPlot(~x1Mask,:) = [];
        
    % remove p2 out of bound
    xg1Plot(:,~x2Mask) = [];
    xg2Plot(:,~x2Mask) = [];
    PsiPlot(:,~x2Mask) = [];

    if(isfield(Options,'contours'))
        contours = Options.contours;
        [~,hc] = contour(ha,xg1Plot,xg2Plot,abs(PsiPlot).^2,contours);
    else
        [~,hc] = contour(ha,xg1Plot,xg2Plot,abs(PsiPlot).^2);
    end

    Options.x1 = '$x_1$';
    Options.x2 = '$x_2$';
    
    fixContourPlot(ha,hc,Options)
    
end