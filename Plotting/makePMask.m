function pMask = makePMask(PsiHat,Numerics)

    cutoff = 10^-2;

    PsiHat = abs(PsiHat);
    
    maxPsiHat = max(PsiHat(:));
    bound = maxPsiHat*cutoff;
    PsiHatMask = ( PsiHat > bound );
    
%    spy(PsiHatMask)
    

    
    p1Mask = sum(PsiHatMask,1);
    p2Mask = sum(PsiHatMask,2);
    
    p1Mask = ( p1Mask > 0 );
    p2Mask = ( p2Mask > 0 );
    
    pMask = [p1Mask(:) p2Mask(:)];
    
%     p1MinPos = find(p1Mask,'first');
%     p1MaxPos = find(p1Mask,'last');
%     p2MinPos = find(p2Mask,'first');
%     p2MaxPos = find(p2Mask,'last');

    
end