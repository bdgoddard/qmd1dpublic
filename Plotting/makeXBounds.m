function [xMin,xMax] = makeXBounds(Psi,Numerics)

    [x,~,~,~] = makeXP(Numerics);
    
    if(isfield(Numerics,'cutoff'))
        cutoff = Numerics.cutoff;
    else
        cutoff = 10^-2;
    end
        

    Psi = abs(Psi).^2;
    
    maxPsi = max(Psi(:));
    bound = maxPsi*cutoff;
    PsiMask = ( Psi > bound );
        
    d = Numerics.d;
    
    if(d==2)
    
        x1Mask = sum(PsiMask,2);
        x2Mask = sum(PsiMask,1);

        x1Mask = x1Mask(:);
        x2Mask = x2Mask(:);

        x1MinPos = find(x1Mask,1,'first');
        x1MaxPos = find(x1Mask,1,'last');
        x2MinPos = find(x2Mask,1,'first');
        x2MaxPos = find(x2Mask,1,'last');

        x1Min = x(x1MinPos);
        x1Max = x(x1MaxPos);
        x2Min = x(x2MinPos);
        x2Max = x(x2MaxPos);

        xMin = [x1Min; x2Min];
        xMax = [x1Max; x2Max];
    
    elseif(d==1)
        xMask = PsiMask;
        xMinPos = find(xMask,1,'first');
        xMaxPos = find(xMask,1,'last');
        xMin = x(xMinPos);
        xMax = x(xMaxPos);
        
    end
        
end