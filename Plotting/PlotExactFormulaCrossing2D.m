function PlotExactFormulaCrossing2D(WPN,Options)

    if(nargin<2)
        Options = struct;
    end

    [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

    figure('Position',[50,50,1200,800]);
    set(0,'defaultaxesfontsize',25);
    set(0,'defaulttextfontsize',25);
    set(0,'defaultaxeslinewidth',1)
    set(0,'defaultlinelinewidth',2)        

    Options.LineWidth = 2;
    
    
    PsiHatF = Wavepackets.FormulaHatCrossing;
    PsiHatE = Wavepackets.ExactHatDownCrossing;
    psiHatFn = Wavepackets.PsiHatUpCrossing;
    PsiHatU = makePsiHatFull(psiHatFn,Wavepackets,Numerics);
    
    Numerics = addXPNumerics(Numerics);

    PsiF = eIFTn(PsiHatF,Numerics);
    PsiU = eIFTn(PsiHatU,Numerics);
    

    % get bounds
    [pMinF,pMaxF] = makePBounds(PsiHatF,Numerics);
    [pMinU,pMaxU] = makePBounds(PsiHatU,Numerics);

    pMin = min(pMinF,pMinU);
    pMax = max(pMaxF,pMaxU);
    
    Options.pMin = pMin;
    Options.pMax = pMax;

    [xMinF,xMaxF] = makeXBounds(PsiF,Numerics);
    [xMinU,xMaxU] = makeXBounds(PsiU,Numerics);

    xMin = min(xMinF,xMinU);
    xMax = max(xMaxF,xMaxU);

    Options.xMin = xMin;
    Options.xMax = xMax;
    
    
    ha = tightsubplot(2,2,[0.13,-0.2],0.1,0);
    
    Options.axes = ha(1);
    Options.title = 'Momentum Space Upper';
    
    PlotPsiHat2D(PsiHatU,Numerics,Options);
    
    
    OptionsSpace = Options;
    OptionsSpace.colorbar = false;
    OptionsSpace.axes = ha(2);
    OptionsSpace.title = 'Physical Space';
    
    OptionsSpace.Colour = 'b';
    OptionsSpace.Style = '--';
    
    PlotPsi2D(PsiU,Numerics,OptionsSpace);

    OptionsSpace.Colour = 'r';
    OptionsSpace.Style = '-';

    hold(ha(2), 'on');
    PlotPsi2D(PsiF,Numerics,OptionsSpace);

    OptionsSpace.Colour = 'k';
    OptionsSpace.Style = ':';

    rho = makeRhoFull(Potentials,Numerics);
    PlotPsi2D(rho,Numerics,OptionsSpace);
    
    
    Options.axes = ha(3);
    Options.title = 'Momentum Space Lower';
    
    PlotPsiHat2D(PsiHatF,Numerics,Options);
    
    Options.axes = ha(4);
    
    Options.title = 'Error';
    PlotPsiHat2D(PsiHatF - PsiHatE,Numerics,Options);
    
    
end